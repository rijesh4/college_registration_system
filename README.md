The College Registration System Project, developed in JAVA, has three important aspects to it: 

	Creating a working college Registration System.

A College Registration System is a system that makes the registration process, and managing of course information along with updating of data simpler. It provides a fluid transaction for individual users with varied authorizations and privileges to get access accordingly. 

	Implementing Object Oriented Design. 
The system strictly uses the Object Oriented Design to get the maximum power of programming from JAVA.  The design basically tries to incorporate Loosely connected, Façade, and Singularity design patters, to make the program more flexible. 




	Using Java Screen Development Libraries. 
The program uses Java Screen Development Libraries such as Swing and Awt, to create simple front end that is completely independent of external libraries and resources. It not only provides a portability benefits to the system, but also makes the program easily accessible to other screen developmental architectures, such as porting the system to Android (xml) or on web (html/css).   
 




