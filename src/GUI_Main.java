/**/
/*
CLASS NAME :  GUI_Main
         
DESCRIPTION :
		This is the first screen created by the program. This acts as the base screen providing the access based on the 
		user type. If the user is Registrar, it takes the user to Login screen for registrar, and likewise for clerk, and 
		students. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class GUI_Main extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    public Container window;

    //Container window;
    private JPanel m_mainPanel;
    private JButton m_registrarButton;
    private JButton m_clerkButton;
    private JButton m_studentButton;

    /**/
    /*
    NAME : Display - 
    SYNOPSIS :
            double getMinuteFieldData( std::string &a_ticker, JMinBarFieldList::MBField a_fieldNum );
                  a_ticker             --> the ticker for which the data is requested.
                  a_fieldNum       	   --> the number of the field.
    DESCRIPTION :

    RETURNS : Returns the requested field value if successful and -999 otherwise.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public GUI_Main(Container window) {

        currentStateNo = MAIN_PANEL;
        this.window = window;
        m_mainPanel = new JPanel();
        m_mainPanel.setLayout(null);

        m_registrarButton = new JButton("Registrar Login");
        m_registrarButton.setBounds(10, 10, 150, 30);
        m_mainPanel.add(m_registrarButton);
        m_registrarButton.addActionListener(this);

        m_clerkButton = new JButton("Clerk Login");
        m_clerkButton.setBounds(10, 60, 150, 30);
        m_clerkButton.addActionListener(this);
        m_mainPanel.add(m_clerkButton);

        m_studentButton = new JButton("Student Login");
        m_studentButton.setBounds(10, 110, 150, 30);
        m_studentButton.addActionListener(this);
        m_mainPanel.add(m_studentButton);
    }

    /**/
    /*
    NAME : Display - 
    SYNOPSIS :
            double getMinuteFieldData( std::string &a_ticker, JMinBarFieldList::MBField a_fieldNum );
                  a_ticker             --> the ticker for which the data is requested.
                  a_fieldNum       	   --> the number of the field.
    DESCRIPTION :

    RETURNS : Returns the requested field value if successful and -999 otherwise.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_mainPanel);
        m_mainPanel.setVisible(true);
        this.setTitle("College Registration System - Main Panel");
    }

    /**/
    /*
    NAME : Display - 
    SYNOPSIS :
            double getMinuteFieldData( std::string &a_ticker, JMinBarFieldList::MBField a_fieldNum );
                  a_ticker             --> the ticker for which the data is requested.
                  a_fieldNum       	   --> the number of the field.
    DESCRIPTION :

    RETURNS : Returns the requested field value if successful and -999 otherwise.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_registrarButton) ) {
            m_mainPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, LOGIN_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_clerkButton) ) {
            m_mainPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, LOGIN_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_studentButton) ) {
            m_mainPanel.setVisible(false);
            exitCode = 2;
            GUI.instance().addTransition(currentStateNo, exitCode, LOGIN_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}

