/**/
/*
CLASS NAME :  StudentMenu_Gui
         
DESCRIPTION :
		This class is responsible for creating screen which provides a student with  all the options available for him to perform at 
		this system. It handles the transition from the the selected work a student needs to do to the the screen that  does that work. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class StudentMenu_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;

    private JPanel m_studentMenuPanel;
    private JButton m_transcriptButton;
    private JButton m_registerButton;
    private JButton m_dropButton;
    private JButton m_studentlogoutButton;
    private JButton m_checkGradeButton;
    private JButton m_accountButton;
    private JButton m_viewScheduleButton;

    /**/
    /*
    NAME : StudentMenu_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public StudentMenu_Gui(Container window) {

        currentStateNo = STUDENT_PANEL;
        this.window = window;
        m_studentMenuPanel = new JPanel();
        m_studentMenuPanel.setLayout(null);

        m_transcriptButton = new JButton("Transcript");
        m_transcriptButton.setBounds(10, 10, 150, 30);
        m_transcriptButton.addActionListener(this);
        m_studentMenuPanel.add(m_transcriptButton);

        m_viewScheduleButton = new JButton("Class Schedule");
        m_viewScheduleButton.setBounds(10, 60, 150, 30);
        m_viewScheduleButton.addActionListener(this);
        m_studentMenuPanel.add(m_viewScheduleButton);

        m_registerButton = new JButton("Register");
        m_registerButton.setBounds(10, 110, 150, 30);
        m_registerButton.addActionListener(this);
        m_studentMenuPanel.add(m_registerButton);

        m_dropButton = new JButton("Drop");
        m_dropButton.setBounds(10, 160, 150, 30);
        m_dropButton.addActionListener(this);
        m_studentMenuPanel.add(m_dropButton);

        m_checkGradeButton = new JButton("Check Grade");
        m_checkGradeButton.setBounds(10, 210, 150, 30);
        m_checkGradeButton.addActionListener(this);
        m_studentMenuPanel.add(m_checkGradeButton);

        m_accountButton = new JButton("Account");
        m_accountButton.setBounds(10, 260, 150, 30);
        m_accountButton.addActionListener(this);
        m_studentMenuPanel.add(m_accountButton);

        m_studentlogoutButton = new JButton("Logout");
        m_studentlogoutButton.setBounds(10, 610, 150, 30);
        m_studentlogoutButton.addActionListener(this);
        m_studentMenuPanel.add(m_studentlogoutButton);
    }

    /**/
    /*
    NAME : display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void display() {
        window.add(m_studentMenuPanel);
        m_studentMenuPanel.setVisible(true);
        setTitle("College Registration System - Student Menu");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                 
    DESCRIPTION :
    		The function handles the onClick events for the Save data screen. The following being the events:
    		transcript:  	transition to view Transcript screen to view current transcript. 
    		viewSchedule:   transition to view Schedule screen to view current schedule of the student
    		register: 		transition to register gui to enable registration to a course. 
    		drop:			transition to drop gui to enable dropping of a course. 
    		checkGrade:		transition to check grade screen to allow student/clerk/registrar to check grade of the student. 
    		account:		transition to account screen to view account details of the student. 
    		logout: 		logout as student. 
    		
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(m_transcriptButton) ) {
            m_studentMenuPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, TRANSCRIPT);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_viewScheduleButton) ) {
            m_studentMenuPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, CLASS_SCHEDULE);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_registerButton) ) {
            m_studentMenuPanel.setVisible(false);
            exitCode = 2;
            GUI.instance().addTransition(currentStateNo, exitCode, REGISTER);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_dropButton) ) {
            m_studentMenuPanel.setVisible(false);
            exitCode = 3;
            GUI.instance().addTransition(currentStateNo, exitCode, DROP);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_checkGradeButton) ) {
            m_studentMenuPanel.setVisible(false);
            exitCode = 4;
            GUI.instance().addTransition(currentStateNo, exitCode, CHECKGRADE);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_accountButton) ) {
            m_studentMenuPanel.setVisible(false);
            exitCode = 5;
            GUI.instance().addTransition(currentStateNo, exitCode, ACCOUNT);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_studentlogoutButton) ) {
            m_studentMenuPanel.setVisible(false);
            if (GUI.instance().clerkAsStudentFlag == true) {
                GUI.instance().clerkAsStudentFlag = false;
                exitCode = 5;
                GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            } else {
                exitCode = 6;
                GUI.instance().addTransition(currentStateNo, exitCode, MAIN_PANEL);
            }
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}
