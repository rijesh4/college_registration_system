/**/
/*
CLASS NAME :  SECTIONLIST
         
DESCRIPTION :
		The class SectionList is responsible for holding data of List of all Sections. It enables 
		adding and removing of new sections to the list of all sections. 
		The List is statically available to be used by the singular college object. It also 
		has access to the file system to update the info from the file sytem, and manipulate it 
		accordingly. 
AUTHOR : Rijesh Kumar 
DATE : 05/04/15 
*/
/**/

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class SectionList implements Serializable {
    // Version ID checked during deserialization
    private static final long serialVersionUID = 1L;

    private ArrayList<Section> m_sections = new ArrayList<Section>();
    private static SectionList sectionList;

    /**/
    /*
    NAME :  SectionList
    SYNOPSIS : 
             
    DESCRIPTION :
			 Constructor is private so that it cannot be called directly.
    		 Must be called through instance method.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    private SectionList() {
    }

    /**/
    /*
    NAME :  SectionList instance()
    SYNOPSIS : 
             
    DESCRIPTION :
			 Singleton to insure only one and only one CourseList instance created.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public static SectionList instance() {

        if (sectionList == null) {
            return (sectionList = new SectionList());
        } else
            return sectionList;
    }

    /**/
    /*
    NAME :  retrieve
    SYNOPSIS : 
             String a_sectionID  --> select the section from the give sectionID
    DESCRIPTION :
			 retrieve the section from the list   
    RETURNS : return the section if the section id is valid otherwise return null
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Section retrieve(String a_sectionID) {
        for (Iterator<Section> iterator = m_sections.iterator(); iterator.hasNext();) {
            Section section = (Section) iterator.next();
            if (section.getSectionID().equals(a_sectionID)) {
                return section;
            }
        }
        return null;
    }

    /**/
    /*
    NAME : remove
    SYNOPSIS : 
            String a_sectionID  --> select the section from the give sectionID
    DESCRIPTION :
			 remove the section from the list   
    RETURNS : return true if the section id is valid after removing the section otherwise return false
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean remove(int a_sectionID) {

        for (Iterator<Section> iterator = m_sections.iterator(); iterator.hasNext();) {
            Section section = (Section) iterator.next();
            if (section.getSectionID().equals(a_sectionID)) {
                iterator.remove();
                return true;
            }

        }
        return false;
    }

    /**/
    /*
    NAME : insert
    SYNOPSIS : 
             Section a_section  --> the section object to be inserted to the list
    DESCRIPTION :
			 adds a new section object to the list    
    RETURNS : return true if the section id is valid and adds a new course
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean insert(Section a_section) {
        m_sections.add(a_section);
        return true;
    }

    /**/
    /*
    NAME : getSections
    SYNOPSIS : 
            
    DESCRIPTION :
			the method returs the list of all sections in the form of an iterator    
    RETURNS : section iterator containing the list of all sections
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Iterator<Section> getSections() {
        return m_sections.iterator();
    }

    /**/
    /*
    NAME :  writeObject
    SYNOPSIS :   
    	     ObjectOutputStream output	--> the object as passed in as parameter by the save method to write to the file. 
    DESCRIPTION :
			method that writes out to a file all the sectionList data.     
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    private void writeObject(java.io.ObjectOutputStream a_output) {
        try {
            a_output.defaultWriteObject();
            a_output.writeObject(sectionList);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

    /**/
    /*
    NAME :  readObject
    SYNOPSIS :   
    	     ObjectInputStream a_input	--> the read object as passed in as parameter by the retrieve method. 
    DESCRIPTION :
			method that that reads in all section list data from a file    
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    private void readObject(java.io.ObjectInputStream a_input) {
        try {
            if (sectionList != null) {
                return;
            } else {
                a_input.defaultReadObject();
                if (sectionList == null) {
                    sectionList = (SectionList) a_input.readObject();
                } else {
                    a_input.readObject();
                }
            }
        } catch (IOException ioe) {
            System.out.println("in SectionList readObject \n" + ioe);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the sections into string.
    RETURNS : String value of the Section info. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String toString() {
        return m_sections.toString();
    }
}
