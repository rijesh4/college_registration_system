/**/
/*
CLASS NAME :  GUI_LOGIN
         
DESCRIPTION :
		This is the GUI class acts as the common login screen for the registrar, clerk, student. Based on the login type the 
		username and password is determined correct/incorrect giving/revoking access to respective authorized accounts. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class GUI_LOGIN extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_loginPanel;
    private JLabel m_directionsLabel;
    private JTextField m_usernameTextField;
    private JLabel m_usernameLabel;
    private JTextField m_passwordTextField;
    private JLabel m_passwordLabel;
    private JButton m_loginButton;
    private JButton m_loginbackButton;
    private JTextField m_result;
   
    
    /**/
    /*
    NAME : GUI_LOGIN 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public GUI_LOGIN(Container window) {

        currentStateNo = LOGIN_PANEL;
        this.window = window;
        m_loginPanel = new JPanel();
        m_loginPanel.setLayout(null);

        m_directionsLabel = new JLabel("Please Enter your username and password.");
        m_directionsLabel.setBounds(10, 10, 250, 30);
        m_loginPanel.add(m_directionsLabel);

        m_usernameTextField = new JTextField();
        m_usernameTextField.setBounds(10, 60, 150, 30);
        m_loginPanel.add(m_usernameTextField);

        m_usernameLabel = new JLabel("username");
        m_usernameLabel.setBounds(200, 60, 150, 30);
        m_loginPanel.add(m_usernameLabel);
        
        //Encrypt password 
        m_passwordTextField = new JPasswordField();
       // m_passwordTextField = new JTextField();
        m_passwordTextField.setBounds(10, 110, 150, 30);
        m_loginPanel.add(m_passwordTextField);

        m_passwordLabel = new JLabel("password");
        m_passwordLabel.setBounds(200, 110, 150, 30);
        m_loginPanel.add(m_passwordLabel);

        m_loginButton = new JButton("Login");
        m_loginButton.setBounds(10, 160, 150, 30);
        m_loginButton.addActionListener(this);
        m_loginPanel.add(m_loginButton);

        m_result = new JTextField();
        m_result.setBounds(10, 220, 300, 300);
        m_loginPanel.add(m_result);

        m_loginbackButton = new JButton("Back");
        m_loginbackButton.setBounds(10, 610, 150, 30);
        m_loginbackButton.addActionListener(this);
        m_loginPanel.add(m_loginbackButton);
    
    }    

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void display() {
        window.add(m_loginPanel);
    	//window.add (jlayer);
        m_loginPanel.setVisible(true);
    	
        setTitle("College Registration System - Login");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            String userName  			--> input value from the usernameTextField
    		String password 			--> input value from the passwordTextField.      
    		Iterator<Student> iterator 	--> Gets a list of student to validate password.     
    DESCRIPTION :
    		The function handles the onClick events for the main login screen. The following being the events:
    		login: based on the user type , registrar/clerk/student check for the validity of the password,
    		enable login. 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_loginButton)) {

            String username;
            String password;	
            username = m_usernameTextField.getText();
            password = m_passwordTextField.getText();
            if (GUI.loginType == 2) {
                if (username.equals("registrar") && password.equals("registrar")) {
                    exitCode = 0;
                    GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
                } else {
                    exitCode = 1;
                    GUI.instance().addTransition(currentStateNo, exitCode, LOGIN_PANEL);
                }
            } else if (GUI.loginType == 3) {
                if (username.equals("clerk") && password.equals("clerk")) {
                    exitCode = 2;
                    GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
                } else
                    exitCode = 1;
            } else if (GUI.loginType == 4) {

            	// Login validation for students. 
                Iterator<Student> iterator = college.instance().getStudentListIterator();
                while (iterator.hasNext()) {
                    Student member = (Student) iterator.next();
                    String id = "" + member.getId();

                    if (username.equals(id) && password.equals(member.getPassword())) {
                        STUDENT_ID = member.getId();
                        exitCode = 3;
                        GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
                        break;
                    } else
                        exitCode = 1;
                }
            } else {
                exitCode = 1;
            }

            m_loginPanel.setVisible(false);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_loginbackButton) ) {
            m_loginPanel.setVisible(false);
            exitCode = 3;
            GUI.instance().addTransition(currentStateNo, exitCode, MAIN_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}
