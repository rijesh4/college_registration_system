/**/
/*
CLASS NAME :  AddSection_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allow adding a section to the system. The adding 
		of section is handled by the registrar gui. 
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class AddSection_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;
    private JPanel m_addSectionPanel;
    private JTextField m_courseIDTextField;
    private JLabel m_courseIDLabel;
    private JTextField m_timeTextField;
    private JLabel m_timeLabel;
    private JTextField m_locationTextField;
    private JLabel m_locationLabel;

    private JTextField m_semesterTextField;
    private JLabel m_semesterLabel;

    private JTextField m_professorTextField;
    private JLabel m_professorLabel;

    private JTextField m_yearTextField;
    private JLabel m_yearLabel;

    private JButton m_addSectionconfirmButton;
    private JTextArea m_resultTextArea;
    private JButton m_addSectionBackButton;

    /**/
    /*
    NAME : AddSection_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/ 
    public AddSection_Gui(Container window) {

        currentStateNo = ADD_SECTION;
        this.window = window;
        m_addSectionPanel = new JPanel();
        m_addSectionPanel.setLayout(null);


        m_courseIDTextField = new JTextField();
        m_courseIDTextField.setBounds(10, 110, 150, 30);
        m_addSectionPanel.add(m_courseIDTextField);

        m_courseIDLabel = new JLabel("Course ID");
        m_courseIDLabel.setBounds(200, 110, 150, 30);
        m_addSectionPanel.add(m_courseIDLabel);


        m_timeTextField = new JTextField();
        m_timeTextField.setBounds(10, 160, 150, 30);
        m_addSectionPanel.add(m_timeTextField);

        m_timeLabel = new JLabel("Time");
        m_timeLabel.setBounds(200, 160, 150, 30);
        m_addSectionPanel.add(m_timeLabel);


        m_locationTextField = new JTextField();
        m_locationTextField.setBounds(10, 210, 150, 30);
        m_addSectionPanel.add(m_locationTextField);

        m_locationLabel = new JLabel("location");
        m_locationLabel.setBounds(200, 210, 150, 30);
        m_addSectionPanel.add(m_locationLabel);


        m_semesterTextField = new JTextField();
        m_semesterTextField.setBounds(10, 260, 150, 30);
        m_addSectionPanel.add(m_semesterTextField);

        m_semesterLabel = new JLabel("semester");
        m_semesterLabel.setBounds(200, 260, 150, 30);
        m_addSectionPanel.add(m_semesterLabel);


        m_professorTextField = new JTextField();
        m_professorTextField.setBounds(10, 310, 150, 30);
        m_addSectionPanel.add(m_professorTextField);

        m_professorLabel = new JLabel("professor");
        m_professorLabel.setBounds(200, 310, 150, 30);
        m_addSectionPanel.add(m_professorLabel);


        m_yearTextField = new JTextField();
        m_yearTextField.setBounds(10, 360, 150, 30);
        m_addSectionPanel.add(m_yearTextField);

        m_yearLabel = new JLabel("year");
        m_yearLabel.setBounds(200, 360, 150, 30);
        m_addSectionPanel.add(m_yearLabel);


        m_addSectionconfirmButton = new JButton("Add Section");
        m_addSectionconfirmButton.setBounds(10, 410, 150, 30);
        m_addSectionconfirmButton.addActionListener(this);
        m_addSectionPanel.add(m_addSectionconfirmButton);

        m_resultTextArea = new JTextArea("Results:");
        m_resultTextArea.setBounds(10, 460, 570, 150);
        m_resultTextArea.setEditable(false);
        m_addSectionPanel.add(m_resultTextArea);

        m_addSectionBackButton = new JButton("Back");
        m_addSectionBackButton.setBounds(10, 610, 150, 30);
        m_addSectionBackButton.addActionListener(this);
        m_addSectionPanel.add(m_addSectionBackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public void display() {
        window.add(m_addSectionPanel);
        m_addSectionPanel.setVisible(true);
        setTitle("University System - Add Section");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            	ActionEvent e 		--> onClick event 
            	String time   		--> the input val from the timeTextField
            	String location   	--> the input val from the locationTextField
            	String CourseID 	--> the input val from the courseIdTextField
            	String semester     --> the input val from the semesterTextField to be parsed into integer value
            	String year  		--> the input val from the yearTextField
            	int professor       --> the input val from the professorTextField.  
                String result 		--> string result of adding the course into college instance. Success/failure 
                    
    DESCRIPTION :
    		The function is responsible for parsing the user input values in the gui fields. 
    		It is also responsible for adding the input values into the college instance, 
    		and add a new section in the system. 
    		It also handles the back pressed button, and takes the user to the previous screen as 
    		handled by  method changeState of GUI class.   

    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_addSectionconfirmButton) ) {

            String courseID = m_courseIDTextField.getText();
            String time = m_timeTextField.getText();
            String location = m_locationTextField.getText();
            String semester = m_semesterTextField.getText();
            String professor = m_professorTextField.getText();
            String year = m_yearTextField.getText();

            String result = college.instance().addSection(courseID, time, location, semester, professor, year);

            m_resultTextArea.setText(result);
        }

        if (e.getSource().equals(m_addSectionBackButton) ) {
            m_addSectionPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}


