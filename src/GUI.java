/**/
/*
CLASS NAME :  GUI
         
DESCRIPTION :
		This class acts as the mediator used for transition from one screen to another. It uses a stateTransTable which is basically
		a matrix 2x2, which accepts current state and exit code to determine which screen the current screen must transition to.      

ASSISTANCE RECEIVED: 
	BY -	Mr. Nitesh K.
		Phone Number  : 201-875- 0256
	For - 
		Helped in understanding the concept of screen transition. 
		Suggested to create a Matrix for transTable for my current purpose. 		
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;

import javax.swing.JFrame;

public class GUI extends JFrame {

    private static final long serialVersionUID = 1L;

    private static GUI gui;
    private static int nextState;
    protected static int loginType;
    protected static String currentuser;
    protected static boolean registrarFlag = false;
    protected static boolean clerkAsStudentFlag = false;

    public Container window;

    private final int WINDOW_WIDTH = 800;
    private final int WINDOW_HEIGHT = 800;

    HeadGui supergui;
 	 
    private static int[][] stateTransTable = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},};


    //  main program execution
    /**/
    /*
    NAME : main- 
    SYNOPSIS :
                  String[] args  --> command line arguments
                  GUI.instance()  --> gets an instance of Static variable GUI
                  
    DESCRIPTION :
    	The main function initiates the program with GUI instance. 

    RETURNS : Void
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public static void main(String[] args) {
        GUI.instance();
        gui.setVisible(true);
        gui.start();
    }

    
    /**/
    /*
    NAME : instance 
    SYNOPSIS :
            gui  --> an instance of the GUI class
    DESCRIPTION :
    		Singleton to insure one and only one GUI instance created.

    RETURNS : Returns one and only instance of the gui.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/
    public static GUI instance() {
        if (gui == null) {
            return gui = new GUI();
        } else {
            return gui;
        }
    }

    /**/
    /*
    NAME : GUI 
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container. 
    DESCRIPTION :
			The constructor of the class that sets default values of the gui instance and initiates current window. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    private GUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("College Registration System");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window = getContentPane();
    }

    /**/
    /*
    NAME : Start - 
    SYNOPSIS :
            supergui - singleton instance of the HeadGui class that gives the code for the window. 
    DESCRIPTION :
			Displays the screen window. 
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void start() {
        supergui = new GUI_Main(window);
        ((GUI_Main) supergui).display();
    }

    /**/
    /*
    NAME : addTransition 
    SYNOPSIS :
            
    DESCRIPTION :
			int a_current State --> current screen code
			int a_exitCode      --> cureent exit code
			int a_newState      --> new value of the mult. dimensional stateTrasTable
    RETURNS : Sets up new value for the stateTransTable. 
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void addTransition(int a_currentState, int a_exitCode, int a_newState) {
        stateTransTable[a_currentState][a_exitCode] = a_newState;
    }

    /**/
    /*
    NAME : changeState 
    SYNOPSIS :
            	int a_current State --> current screen code  
				int a_exitCode      --> cureent exit code 
    DESCRIPTION :
			Based on the cureentState code and the exit code the function is responsible 
			for swapping the singleton instance of the supergui with the new gui, 
			setting up new window to be displayed. 
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void changeState(int a_currentState, int a_exitCode) {
        if ((a_currentState == 0) && (a_exitCode == 0))
            loginType = 2;
        if ((a_currentState == 0) && (a_exitCode == 1))
            loginType = 3;
        if ((a_currentState == 0) && (a_exitCode == 2))
            loginType = 4;

        nextState = stateTransTable[a_currentState][a_exitCode];

        switch (nextState) {
            case 0:
                supergui = new GUI_Main(window);
                ((GUI_Main) supergui).display();
                break;
            case 1:
                supergui = new GUI_LOGIN(window);
                ((GUI_LOGIN) supergui).display();
                break;
            case 2:
                supergui = new RegistrarGuiMenu(window);
                ((RegistrarGuiMenu) supergui).display();
                break;
            case 3:
                supergui = new ClerkMenu_Gui(window);
                ((ClerkMenu_Gui) supergui).display();
                break;

            case 4:
                supergui = new StudentMenu_Gui(window);
                ((StudentMenu_Gui) supergui).display();
                break;

            case 5:
                supergui = new AddStudent_Gui(window);
                ((AddStudent_Gui) supergui).display();
                break;

            case 6:
                supergui = new AddCourse_Gui(window);
                ((AddCourse_Gui) supergui).display();
                break;

            case 7:
                supergui = new AddSection_Gui(window);
                ((AddSection_Gui) supergui).display();
                break;

            case 8:
                supergui = new ViewSectionRoster_Gui(window);
                ((ViewSectionRoster_Gui) supergui).display();
                break;

            case 11:
                supergui = new SaveData_Gui(window);
                ((SaveData_Gui) supergui).display();
                break;

            case 12:
                supergui = new RetrieveData_Gui(window);
                ((RetrieveData_Gui) supergui).display();
                break;

            case 13:
                supergui = new ViewSectionRoster_Gui(window);
                ((ViewSectionRoster_Gui) supergui).display();
                break;

            case 14:
                supergui = new EditGrade_Gui(window);
                ((EditGrade_Gui) supergui).display();
                break;

            case 15:
                supergui = new ViewTranscript_Gui(window);
                ((ViewTranscript_Gui) supergui).display();
                break;

            case 16:
                supergui = new Register_Gui(window);
                ((Register_Gui) supergui).display();
                break;

            case 17:
                supergui = new Drop_Gui(window);
                ((Drop_Gui) supergui).display();
                break;

            case 18:
                supergui = new ViewAccount_Gui(window);
                ((ViewAccount_Gui) supergui).display();
                break;

            case 19:
                supergui = new CheckGrade_Gui(window);
                ((CheckGrade_Gui) supergui).display();
                break;

            case 20:
                supergui = new ClerkLoginAsStudent_Gui(window);
                ((ClerkLoginAsStudent_Gui) supergui).display();
                break;

            case 21:
                supergui = new ViewClassSchedule_Gui(window);
                ((ViewClassSchedule_Gui) supergui).display();
                break;

            case 22:
                supergui = new MassEditGrade_Gui(window);
                ((MassEditGrade_Gui) supergui).display();
                break;

            case 23:
                supergui = new GenerateTuitionBill_Gui(window);
                ((GenerateTuitionBill_Gui) supergui).display();
                break;

            case 24:
                supergui = new EditSectionGrades_Gui(window);
                ((EditSectionGrades_Gui) supergui).display();
                break;
        }
    }
}