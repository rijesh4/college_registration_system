/**/
/*
CLASS NAME :  TRANSCRIPT
         
DESCRIPTION :
		This class maintains the class and grade of a student. 
AUTHOR : Rijesh Kumar 
DATE : 05/01/15 
*/
/**/

import java.io.Serializable;
 
public class Transcript implements Serializable {

    private static final long serialVersionUID = 1L;

    private String m_sectionID;
    private String m_grade;
    private String m_location;
    private String m_time;
    private String m_semester;
    private String m_course;
    private String m_year;

    /**/
    /*
    NAME :  Transcript
    SYNOPSIS : 
    	String SectionID	--> sectionId of the course in the transcript  
    	String location		--> location where the course was taken 
    	String time			--> time of the course 
    	String semester		--> semester the course taken
    	String course		--> description
    	String year			--> year
             
    DESCRIPTION :
			The Constructor is responsible for adding course object in the transcript using the given parameters.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public Transcript(String sectionID, String location, String time, String semester, String course, String year) {
        this.m_sectionID = sectionID;
        this.m_grade = "*";
        this.m_location = location;
        this.m_time = time;
        this.m_semester = semester;
        this.m_course = course;
        this.m_year = year;
    }

    /**/
    /*
    NAME :  getYear
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_year.  
    RETURNS : String m_year.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getYear() {
        return m_year;
    }
    
    /**/
    /*
    NAME :  setYear
    SYNOPSIS : 
    	   String year  --> updating m_year         
    DESCRIPTION :
			setter for m_year.  
    RETURNS : void
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setYear(String year) {
        this.m_year = year;
    }
    
    /**/
    /*
    NAME :  getLocation
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_location.  
    RETURNS : string m_location
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getLocation() {
        return m_location;
    }

    /**/
    /*
    NAME :  setLocation
    SYNOPSIS : 
    	   string location            
    DESCRIPTION :
			setter for m_location.  
    RETURNS : none
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setLocation(String location) {
        this.m_location = location;
    }

    /**/
    /*
    NAME :  getTime
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_time.  
    RETURNS : String .
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getTime() {
        return m_time;
    }

    /**/
    /*
    NAME :  setTime
    SYNOPSIS : 
    	    String time         
    DESCRIPTION :
			setter for m_time.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setTime(String time) {
        this.m_time = time;
    }

    /**/
    /*
    NAME :  getSemester
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_semester.  
    RETURNS : String m_semester.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getSemester() {
        return m_semester;
    }

    /**/
    /*
    NAME :  setSemester
    SYNOPSIS : 
    	    String semester
    DESCRIPTION :
			setter for m_semester.  
    RETURNS : .
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setSemester(String semester) {
        this.m_semester = semester;
    }

    /**/
    /*
    NAME :  getCourse
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_course.  
    RETURNS : String m_sectionCount.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getCourse() {
        return m_course;
    }

    /**/
    /*
    NAME :  setCourse
    SYNOPSIS : 
    	     String m_sectionCount.        
    DESCRIPTION :
			setter for m_course.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setCourse(String course) {
        this.m_course = course;
    }

    /**/
    /*
    NAME :  getSectionID
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_sectionID.  
    RETURNS : String m_sectionID.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getSectionID() {

        return m_sectionID;
    }

    /**/
    /*
    NAME :  setSectionID
    SYNOPSIS : 
    	      String m_sectionID.       
    DESCRIPTION :
			setter for m_sectionID.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setSectionID(String sectionID) {
        this.m_sectionID = sectionID;
    }

    /**/
    /*
    NAME :  getGrade
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_grade.  
    RETURNS : Strin m_grade.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String getGrade() {
        return m_grade;
    }

    /**/
    /*
    NAME :  setGrade
    SYNOPSIS : 
    	      String grade.       
    DESCRIPTION :
			setter for m_grade.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public void setGrade(String grade) {
        this.m_grade = grade;
    }

    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the transcript into string, which is overriden to 
			be displayed on the screen too.    
    RETURNS : String 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    @Override
    public String toString() {
        return "Transcript{" +
                "sectionID=" + m_sectionID +
                ", grade='" + m_grade + '\'' +
                ", location='" + m_location + '\'' +
                ", time='" + m_time + '\'' +
                ", semester='" + m_semester + '\'' +
                ", course='" + m_course + '\'' +
                ", year='" + m_year + '\'' +
                '}';
    }
}
