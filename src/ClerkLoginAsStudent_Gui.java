/**/
/*
CLASS NAME :  ClerkLoginAsStudent_Gui
         
DESCRIPTION :
		This is the GUI class allows transition from a clerk gui to a students gui to enable a clerk to
		access a student's acount and manipulate its respective data.  
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ClerkLoginAsStudent_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_loginPanel;
    private JLabel m_directionsLabel;
    private JTextField m_studentIDTextField;
    private JLabel m_studentIDLabel;

    private JButton m_loginButton;
    private JButton m_loginbackButton;

    private JTextField m_result;

    /**/
    /*
    NAME : ClerkLoginAsStudent_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public ClerkLoginAsStudent_Gui(Container window) {

        currentStateNo = CLERK_AS_STUDENT;
        this.window = window;
        m_loginPanel = new JPanel();
        m_loginPanel.setLayout(null);

        m_directionsLabel = new JLabel("Please Enter a Student ID.");
        m_directionsLabel.setBounds(10, 10, 250, 30);
        m_loginPanel.add(m_directionsLabel);

        m_studentIDTextField = new JTextField();
        m_studentIDTextField.setBounds(10, 60, 150, 30);
        m_loginPanel.add(m_studentIDTextField);

        m_studentIDLabel = new JLabel("Student ID");
        m_studentIDLabel.setBounds(200, 60, 150, 30);
        m_loginPanel.add(m_studentIDLabel);


        m_loginButton = new JButton("Login");
        m_loginButton.setBounds(10, 110, 150, 30);
        m_loginButton.addActionListener(this);
        m_loginPanel.add(m_loginButton);

        m_result = new JTextField();
        m_result.setBounds(10, 220, 300, 300);
        m_loginPanel.add(m_result);

        m_loginbackButton = new JButton("Back");
        m_loginbackButton.setBounds(10, 610, 150, 30);
        m_loginbackButton.addActionListener(this);
        m_loginPanel.add(m_loginbackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/  
    public void display() {
        window.add(m_loginPanel);
        m_loginPanel.setVisible(true);
        setTitle("College Registration System - Clerk Login As Student");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            	String studentId 			--> gets the student Id from the studentIdTextField 
            	Student member				--> Stores the student object from the iterator. 
            	String id					-->  temporarily stores id to be parsed into int val. 
            	Iterator<Student> iterator 	--> stores the Student elements to be traversed to find the 
            	 								    individual elements by the use of iterator container. 
            	
                    
    DESCRIPTION :
    		The function is responsible for catching the login button event. Since there student 
    		and clerk can both access a student's account, the addTransition method is called to 
    		get the required screen transition. 
    		It does that by getting the studentListIterator, that iterates the id of each student 
    		and calls the method addTransition on the singular gui instance. 

    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/      
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_loginButton) ) {

            String studentID;

            studentID = m_studentIDTextField.getText();

            Iterator<Student> iterator = college.instance().getStudentListIterator();
            while (iterator.hasNext()) {
                Student member = (Student) iterator.next();
                String id = "" + member.getId();

                if (studentID.equals(id)) {
                    STUDENT_ID = Integer.parseInt(id);
                    exitCode = 0;
                    GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
                    break;
                } else {
                    exitCode = 1;
                    GUI.instance().addTransition(currentStateNo, exitCode, CLERK_AS_STUDENT);
                }
            }
            if (exitCode == CLERK_AS_STUDENT)
                m_result.setText("Student was not found");

            m_loginPanel.setVisible(false);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_loginbackButton) ) {
            m_loginPanel.setVisible(false);
            exitCode = 2;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
