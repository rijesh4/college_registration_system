/**/
/*
CLASS NAME :  COURSELIST
         
DESCRIPTION :
		The class CourseList is responsible for holding data of List of all courses. 
		The List is statically available to be used by the singular college object. It also 
		has access to the file system to update the info from the file sytem, and manipulate it 
		accordingly. 
AUTHOR : Rijesh Kumar 
DATE : 05/04/15 
*/
/**/

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class CourseList implements Serializable {
    // Version ID checked during deserialization
    private static final long serialVersionUID = 1L;
    private ArrayList<Course> m_courses = new ArrayList<Course>();
    private static CourseList m_courseList;

    
    /**/
    /*
    NAME :  CourseList
    SYNOPSIS : 
             
    DESCRIPTION :
			 Constructor is private so that it cannot be called directly.
    		 Must be called through instance method.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    private CourseList() {
    }

    /**/
    /*
    NAME :  CourseList instance()
    SYNOPSIS : 
             
    DESCRIPTION :
			 Singleton to insure only one and only one CourseList instance created.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public static CourseList instance() {

        if (m_courseList == null) {
            return (m_courseList = new CourseList());
        } else
            return m_courseList;
    }

    /**/
    /*
    NAME :  retrieve
    SYNOPSIS : 
             String courseID  --> select the course from the give courseID
    DESCRIPTION :
			 retrieve the course from the list   
    RETURNS : return the course if the course id is valid otherwise return null
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Course retrieve(String courseID) {
        for (Iterator<Course> iterator = m_courses.iterator(); iterator.hasNext();) {
            Course course = (Course) iterator.next();
            if (course.getCourseID().equals(courseID)) {
                return course;
            }
        }
        return null;
    }

    /**/
    /*
    NAME : remove
    SYNOPSIS : 
             String courseID  --> select the course from the give courseID
    DESCRIPTION :
			 remove the course from the list   
    RETURNS : return true if the course id is valid after removing the course otherwise return false
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean remove(String courseID) {
        //int count = 0;
        for (Iterator<Course> iterator = m_courses.iterator(); iterator.hasNext();) {
            Course course = (Course) iterator.next();
            if (course.getCourseID().equals(courseID)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    /**/
    /*
    NAME : insert
    SYNOPSIS : 
             Course course  --> the course object to be inserted to the list
    DESCRIPTION :
			 adds a new course object to the list    
    RETURNS : return true if the course id is valid and adds a new course
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean insert(Course course) {
        m_courses.add(course);
        return true;
    }
    
    /**/
    /*
    NAME : getCourses
    SYNOPSIS : 
            
    DESCRIPTION :
			the method returs the list of all courses in the form of an iterator    
    RETURNS : course iterator containing the list of all courses
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Iterator<Course> getCourses() {
        return m_courses.iterator();
    }

    /**/
    /*
    NAME :  writeObject
    SYNOPSIS :   
    	     ObjectOutputStream output	--> the object as passed in as parameter by the save method to write to the file. 
    DESCRIPTION :
			method that writes out to a file all the courseList data.     
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    private void writeObject(java.io.ObjectOutputStream output) {
        try {
            output.defaultWriteObject();
            output.writeObject(m_courseList);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

    /**/
    /*
    NAME :  readObject
    SYNOPSIS :   
    	     ObjectInputStream input	--> the read object as passed in as parameter by the retrieve method. 
    DESCRIPTION :
			method that that reads in all course list data from a file    
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    private void readObject(java.io.ObjectInputStream input) {
        try {
            if (m_courseList != null) {
                return;
            } else {
                input.defaultReadObject();
                if (m_courseList == null) {
                    m_courseList = (CourseList) input.readObject();
                } else {
                    input.readObject();
                }
            }
        } catch (IOException ioe) {
            System.out.println("in CourseList readObject \n" + ioe);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the courses into string.
    RETURNS : String value of the course info. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String toString() {
        return m_courses.toString();
    }
}
