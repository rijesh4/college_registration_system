
/**/
/*
CLASS NAME :  COLLEGE
         
DESCRIPTION :
		This is the College class that basically controls all the other classes besides
  		the user interface class.  It will interact with the StudentList, CourseList,
  		SecitonList, and almost all the rest of the classes.  The user interface
  		calls a function in College, and then this class will perform the desired
  		function based on that call.  This class is also responsible for returning
  		any results or items back to the user interface.  
AUTHOR : Rijesh Kumar 
DATE : 05/05/15 
*/
/**/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;

public class College implements Serializable {
    private static final long serialVersionUID = 1L;
    //declare a variable for each of the lists and the university
    private StudentList m_studentList;
    private CourseList m_courseList;
    private SectionList m_sectionList;
    private static College college;

    /**/
    /*
    NAME :  College
    SYNOPSIS : 
    	     
    DESCRIPTION :
			constructor that makes sure there is only one instance of each private variable  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    private College() {
        m_studentList = StudentList.instance();
        m_courseList = CourseList.instance();
        m_sectionList = SectionList.instance();
    }

    /**/
    /*
    NAME :  College instance()
    SYNOPSIS : 
    	     
    DESCRIPTION :
			constructor that makes sure there is only one instance of College  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public static College instance() {
        if (college == null) {
            StudentIdServer.instance();
            return (college = new College());
        } else {
            return college;
        }
    }

    /**/
    /*
    NAME :  addStudent
    SYNOPSIS : 
    	     String name		--> name of the student to be added to the student list.  
    	     String address		--> address of the student to be added to the student list
    	     String phone		--> phone number of the  student to be added to the student list. 
    	     boolean hold		--> checks if any holds on students account 
    DESCRIPTION :
			method that adds a student to the student list  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String addStudent(String name, String address, String phone, boolean hold) {
        Student student = new Student(name, address, phone, 0.0, hold);
        if (m_studentList.insert(student)) {
            return student.toString();
        } else {

            return "Student Not Added.";
        }
    }

    /**/
    /*
    NAME :  addCourse
    SYNOPSIS : 
    	     String CourseName		--> name of the course to be added to the course list.  
    	     String deptName		--> deptName of the course to be added to the course list.
    	     int Credits			--> number of credits of the course to be added to the course list. 
    	     String description		--> description of the course to be added to the course list.
    DESCRIPTION :
			method that adds a course to the course list  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String addCourse(String courseName, String deptName, String courseID, int credits, String description) {
        Course course = new Course(courseName, deptName, courseID, credits, description);
        if (m_courseList.insert(course)) {
            return course.toString();
        } else {
            return "Course not Added.";
        }
    }

    /**/
    /*
    NAME :  addSection
    SYNOPSIS : 
    	     String CourseID		--> courseId of the section  
    	     String name			--> name of the section.
    	     String location		--> location of the section. 
    	     String semester		--> the semester the section is being offered.
    	     String professor		--> professor teaching the current section
    	     String year			--> year the section is offered.  
    DESCRIPTION :
			method that adds a section to the section list  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String addSection(String courseID, String time, String location, String semester, String professor, String year) {
        //a course is needed here so that we know which one the section belongs to
        Course course = m_courseList.retrieve(courseID);

        if (course == null)
            return " Error. The course id does not exits ";
        int count = course.getSectionCount() + 1; //count of how many sections the course has
        course.setSectionCount(count); // resets the section count
        Section section = new Section(time, location, professor, courseID, course.getCourseName(), semester, year, count);
        if (m_sectionList.insert(section)) {
            return (section.toString());
        } else {
            return "Error.Section Not Added .";
        }
    }

    /**/
    /*
    NAME :  registerForSection
    SYNOPSIS : 
    	     int studentID			--> studentId to retrieve student data from the student list
    	     String sectionId		--> sectionId to retrieve section data from the section list
    DESCRIPTION :
			method that registers a student to a section. 
			Checks if the student and section exists. It also checks if there exists any hold
			on students account. Else registers a student to a course, and updates the billing info. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public int registerForSection(int studentID, String sectionID) {
        //retrieve the desired student, seciton, and course
        Section section = m_sectionList.retrieve(sectionID);
        Student student = m_studentList.retrieve(studentID);
        //   Course course = courseList.retrieve(section.getCourseID())  ;
        //if the section or student does not exist
        if (student == null) {
            return 1;
        }
        if (section == null) {
            return 2;
        }
        Course course = m_courseList.retrieve(section.getCourseID());
        if (student.isHolds()) {
            return 3;
        }
        double fee = course.getTotalCost();
        //add the section to the student's transcript
        if (student.addSectionToStudentTranscript(sectionID, section.getLocation(), section.getTime(), section.getSemester(),
                section.getCourseName(), section.getYear()) == false) {
            return 4;
        }
        if (section.insertStudent(studentID, student.getName()) == false) {
            return 5;
        }
        student.updateAmountDue(fee);
        return 0;
    }

    /**/
    /*
    NAME :  dropFromSection
    SYNOPSIS : 
    	     int studentID			--> studentId to retrieve student data from the student list
    	     String sectionId		--> sectionId to retrieve section data from the section list
    DESCRIPTION :
			method that drops a student from a section. 
			Checks if the student and section exists. Else drops the student from the course, and updates the billing info. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public int dropFromSection(int studentID, String sectionID) {
        Section section = m_sectionList.retrieve(sectionID);
        Student student = m_studentList.retrieve(studentID);
        //make sure the student and the section exist
        if (section == null) {
            return 2;
        }
        if (student == null) {
            return 1;
        }
        Course course = m_courseList.retrieve(section.getCourseID());
        double fee = course.getTotalCost();

        if (student.removeSectionFromTranscript(sectionID) == false) {
            return 6;
        }
        if (section.removeStudent(studentID) == false) {
            return 7;
        }
        student.updateAmountDue(0 - fee);    //make the fee negative to decrement the student's amount due
        return 0;
    }

    /**/
    /*
    NAME :  getTranscriptIterator
    SYNOPSIS : 
    	     int studentID			--> studentId to retrieve student data from the student list
    DESCRIPTION :
			method that returns a list of elements in the transcript of a student.  
    RETURNS : Iterator<Transcript> iterator if the student exist as checked by given
    		  studentID. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    Iterator<Transcript> getTranscriptIterator(int studentID) {
        //retrieve the student
        Student student = m_studentList.retrieve(studentID);
        //make sure the student exists
        if (student == null) {
            return null;
        }
        //call function that displays all the sections on a student's transcript
        Iterator<Transcript> iterator = student.transcriptIterator();
        return iterator;
    }

    /**/
    /*
    NAME :  getStudentListIterator()
    SYNOPSIS : 
    	    
    DESCRIPTION :
			method that returns a list of students from the student list.
			Which in turn is used to generate tuition bills for all students with outstanding balances   
    RETURNS : Iterator<Student> iterator 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    Iterator<Student> getStudentListIterator() {
        Iterator<Student> firstIt = m_studentList.getStudents();
        return firstIt;
    }

    /**/
    /*
    NAME :  studentGrade
    SYNOPSIS : 
    		Student student		--> studentId to select the student
    		String sectionId	--> sectionId to select the section
    		String grade		--> grade to be entered for a particular student in a particular section. 
    	    
    DESCRIPTION :
			method that enters a section grade for a single student   
    RETURNS : int value of :
    			1 : if student doesnt exist
    			2: if section doesnt exist
    			0: if the grade change was successful
    			else : returns 8 which is the return value to take the user 
    			to the previous screen. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public int studentGrade(int studentID, String sectionID, String grade) {
        //retrieve the student
        Student student = m_studentList.retrieve(studentID);
        Section section = m_sectionList.retrieve(sectionID);
        //make sure the student exists
        if (student == null) {
            return 1;
        }
        //make sure the section exists
        if (section == null) {
            return 2;
        }
        //function that changes the section grade for a single student
        if (student.setGrade(sectionID, grade))
            return 0;
        else
            return 8;
    }

    /**/
    /*
    NAME :  getSectionRosterIterator
    SYNOPSIS : 
    	   String sectionID  --> sectionId to retrieve the section information from the sectionList.  
    DESCRIPTION :
			method that returns an iterator for entering grades for every student in a section   
    RETURNS : Iterator<SectionRoster> iterator 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public Iterator<SectionRoster> getSectionRosterIterator(String sectionID) {
        //retrieve the seciton
        Section section = m_sectionList.retrieve(sectionID);
        //make sure the section exists
        if (section == null) {
            System.out.println("That section does NOT exist.");
            return null;
        }
        //get the iterator of the section roster
        Iterator<SectionRoster> iterator = section.returnStudentListIterator();
        //return iterator to the user interface
        return iterator;
    }

    /**/
    /*
    NAME :  retrieve
    SYNOPSIS : 
    	     FileInputStream file 		--> fileInputStream to read data from the file ("CollegeData")  
    	     ObjectInputStream input	--> the read object 
    DESCRIPTION :
			method that retrieves any saved college data. The read object is used to gather the college information
			as the object is returned accordingly. Used readObject method from this class.    
    RETURNS : College object from the read file if it exists else returns null forcing the calling method to 
    		  create a new instance of the college. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public static College retrieve() {
        try {
            FileInputStream file = new FileInputStream("CollegeData");
            ObjectInputStream input = new ObjectInputStream(file);
            input.readObject();
            StudentIdServer.retrieve(input);
            return college;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
            return null;
        }
    }

    /**/
    /*
    NAME :  save
    SYNOPSIS : 
    	     FileInputStream file 		--> fileInputStream to read data from the file ("CollegeData")  
    	     ObjectInputStream input	--> the read object 
    DESCRIPTION :
			function that saves all univeristy data. Used writeObject method from this class.    
    RETURNS : boolean: true if the save was successful else false.  
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public static boolean save() {
        try {
            FileOutputStream file = new FileOutputStream("CollegeData");
            ObjectOutputStream output = new ObjectOutputStream(file);
            output.writeObject(college);
            output.writeObject(StudentIdServer.instance());
            output.close();
            return true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        }
    }

    /**/
    /*
    NAME :  writeObject
    SYNOPSIS :   
    	     ObjectOutputStream output	--> the object as passed in as parameter by the save method to write to the file. 
    DESCRIPTION :
			method that writes out to a file all the college data.     
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    private void writeObject(java.io.ObjectOutputStream output) {
        try {
            output.defaultWriteObject();
            output.writeObject(college);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

    /**/
    /*
    NAME :  readObject
    SYNOPSIS :   
    	     ObjectInputStream input	--> the read object as passed in as parameter by the retrieve method. 
    DESCRIPTION :
			method that that reads in all college data from a file    
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    private void readObject(java.io.ObjectInputStream input) {
        try {
            input.defaultReadObject();
            if (college == null) {
                college = (College) input.readObject();
            } else {
                input.readObject();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**/
    /*
    NAME :  getStudent
    SYNOPSIS : 
    	 int studentId   --> parameter to assist in getting the student object from student list.             
    DESCRIPTION :
			getter for student given the studentID.  
    RETURNS : Student student
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public Student getStudent(int studentID) {
        Student student = m_studentList.retrieve(studentID);
        return student;
    }

    /**/
    /*
    NAME :  getSectionCount
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_sectionCount.  
    RETURNS : int sectionCount.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public Section getSection(String sectionID) {
        Section section = m_sectionList.retrieve(sectionID);
        return section;
    }
}
