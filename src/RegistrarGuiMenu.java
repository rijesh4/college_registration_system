/**/
/*
CLASS NAME :  RegistrarGuiMenu
         
DESCRIPTION :
		This class is responsible for creating screen for all the options available for a registrar. It handles the transition 
		from the the selected work a registrar needs to do to the the screen that  does that work. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class RegistrarGuiMenu extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;
    private JPanel m_registrarMenuPanel;
    private JButton m_addStudentButton;
    private JButton m_addcourseButton;
    private JButton m_addsectionButton;
    private JButton m_studentListButton;
    private JButton m_courselistButton;
    private JButton m_sectionlistButton;
    private JButton m_savedataButton;
    private JButton m_retrievedataButton;
    private JButton m_registrarLogoutButton;
    private JButton m_loginAsClerkButton;
    private JButton m_shutDownButton;
    private JButton m_generateTuitionButton;

    /**/
    /*
    NAME : RegistrarGuiMenu 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public RegistrarGuiMenu(Container window) {
        currentStateNo = REGISTRAR_PANEL;
        this.window = window;
        m_registrarMenuPanel = new JPanel();
        m_registrarMenuPanel.setLayout(null);

        m_addcourseButton = new JButton("Add Course");
        m_addcourseButton.setBounds(10, 10, 150, 30);
        m_addcourseButton.addActionListener(this);
        m_registrarMenuPanel.add(m_addcourseButton);

        m_addsectionButton = new JButton("Add Section");
        m_addsectionButton.setBounds(10, 60, 150, 30);
        m_addsectionButton.addActionListener(this);
        m_registrarMenuPanel.add(m_addsectionButton);

        m_savedataButton = new JButton("Save Data");
        m_savedataButton.setBounds(10, 110, 150, 30);
        m_savedataButton.addActionListener(this);
        m_registrarMenuPanel.add(m_savedataButton);

        m_retrievedataButton = new JButton("Retrieve Data");
        m_retrievedataButton.setBounds(10, 160, 150, 30);
        m_retrievedataButton.addActionListener(this);
        m_registrarMenuPanel.add(m_retrievedataButton);

        m_generateTuitionButton = new JButton("Generate Tuition");
        m_generateTuitionButton.setBounds(10, 210, 150, 30);
        m_generateTuitionButton.addActionListener(this);
        m_registrarMenuPanel.add(m_generateTuitionButton);

        m_loginAsClerkButton = new JButton("Login as Clerk");
        m_loginAsClerkButton.setBounds(10, 260, 150, 30);
        m_loginAsClerkButton.addActionListener(this);
        m_registrarMenuPanel.add(m_loginAsClerkButton);

        m_shutDownButton = new JButton("shutdown");
        m_shutDownButton.setBounds(10, 310, 150, 30);
        m_shutDownButton.addActionListener(this);
        m_registrarMenuPanel.add(m_shutDownButton);

        m_registrarLogoutButton = new JButton("Logout");
        m_registrarLogoutButton.setBounds(10, 610, 150, 30);
        m_registrarLogoutButton.addActionListener(this);
        m_registrarMenuPanel.add(m_registrarLogoutButton);


    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public void display() {
        window.add(m_registrarMenuPanel);
        m_registrarMenuPanel.setVisible(true);
        setTitle("College Registration System - Registrar Menu");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                 
    DESCRIPTION :
    		The function handles the onClick events for the Registrar's Gui screen. The following being the events:
    		addCourse:  Adds a new course to the database. 
    		addSection: Adds a new Section to the database. 
    		saveData: Saves the data from the system to the database
    		retrieveData: Retrieves the data from the database. 
    		generateTuition: Generates tuitions for the students based on the courses registered. 
    		loginAsClerk: enables logging in as a clerk to the registrar. 
    		shutDown: exit program
    		logOut: logout as registrar  
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == m_addcourseButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, ADD_COURSE);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource() == m_addsectionButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, ADD_SECTION);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource() == m_savedataButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 2;
            GUI.instance().addTransition(currentStateNo, exitCode, SAVE_DATA);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource() == m_retrievedataButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 3;
            GUI.instance().addTransition(currentStateNo, exitCode, RET_DATA);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource() == m_generateTuitionButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 4;
            GUI.instance().addTransition(currentStateNo, exitCode, GENERATE_TUITION);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource() == m_loginAsClerkButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 5;
            GUI.instance().registrarFlag = true;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource() == m_shutDownButton) {
            m_registrarMenuPanel.setVisible(false);
            System.exit(0);
        }

        if (e.getSource() == m_registrarLogoutButton) {
            m_registrarMenuPanel.setVisible(false);
            exitCode = 6;
            GUI.instance().addTransition(currentStateNo, exitCode, MAIN_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}

