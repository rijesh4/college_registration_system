/**/
/*
CLASS NAME :  MassEditGrade_Gui
         
DESCRIPTION :
		This class enables a clerk to edit grade of all the students in a section. This is a easier way to 
		enter grades of student at the end of semester so as to not look up each student individually. 
		 The class also handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MassEditGrade_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_massEditGradesPanel;
    private JTextField m_resultTextField;

    private JTextField m_sectionIDTextField;
    private JLabel m_sectionIDLabel;
    private JButton m_massEditGradesButton;
    private JButton m_massEditGradesBackButton;

    /**/
    /*
    NAME : MassEditGrade_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public MassEditGrade_Gui(Container window) {

        currentStateNo = MASS_EDIT_GRADE;
        this.window = window;
        m_massEditGradesPanel = new JPanel();
        m_massEditGradesPanel.setLayout(null);

        m_sectionIDTextField = new JTextField();
        m_sectionIDTextField.setBounds(10, 110, 150, 30);
        m_massEditGradesPanel.add(m_sectionIDTextField);

        m_sectionIDLabel = new JLabel("Section ID");
        m_sectionIDLabel.setBounds(200, 110, 150, 30);
        m_massEditGradesPanel.add(m_sectionIDLabel);

        m_massEditGradesButton = new JButton("Edit Grades");
        m_massEditGradesButton.setBounds(10, 160, 150, 30);
        m_massEditGradesButton.addActionListener(this);
        m_massEditGradesPanel.add(m_massEditGradesButton);

        m_resultTextField = new JTextField();
        m_resultTextField.setBounds(10, 220, 300, 300);
        m_massEditGradesPanel.add(m_resultTextField);

        m_massEditGradesBackButton = new JButton("Back");
        m_massEditGradesBackButton.setBounds(10, 610, 150, 30);
        m_massEditGradesBackButton.addActionListener(this);
        m_massEditGradesPanel.add(m_massEditGradesBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_massEditGradesPanel);
        m_massEditGradesPanel.setVisible(true);
        setTitle("College Registration System - Mass Edit Grades For a Section");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            String sectionID  			--> input value from the sectionIdTextField
    		int result 					--> stores the value returned from the method getSection from the college instance. 
    		      							The value being sectionId or Null.
    		String message 				--> message to be displayed.      
    DESCRIPTION :
    		The function handles the onClick events for the MassEditGrade section screen. The following being the events:
    		massEditGrade: edit grade for a student in a particular section 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {


        if (e.getSource().equals(m_massEditGradesButton) ) {
            String sectionID = m_sectionIDTextField.getText();

            Section result = college.instance().getSection(sectionID);

            String message = "";

            if (result == null) {
                message += "That section does NOT exist.";
                m_resultTextField.setText(message);
            } else {
                SECTION_ID = sectionID;
                m_massEditGradesPanel.setVisible(false);
                exitCode = 0;
                GUI.instance().addTransition(currentStateNo, exitCode, EDIT_GRADES_SECTION);
                GUI.instance().changeState(currentStateNo, exitCode);
            }
        }

        if (e.getSource().equals(m_massEditGradesBackButton) ) {
            m_massEditGradesPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
