/**/
/*
CLASS NAME :  COURSE
         
DESCRIPTION :
		The class Course is responsible for holding a course data, and interact with 
		College class when needed to update the UI.   
AUTHOR : Rijesh Kumar 
DATE : 05/05/15 
*/
/**/
import java.io.Serializable;
public class Course implements Serializable {

    private static final long serialVersionUID = 001L;


    private String m_courseName;
    private String m_deptName;
    private String m_courseID;
    private int m_numberOfCredits;
    private String m_description;
    private int m_sectionCount = 0;
    private static final int costPerCredit = 200;
    
    
    /**/
    /*
    NAME :  Course
    SYNOPSIS : 
    	String courseName		--> course name 
    	String deptName			--> dept name
    	String courseId			--> course Id
    	int numberOfCredits		--> number of credits
    	String description		--> description
             
    DESCRIPTION :
			The Constructor is responsible for creating course object using the given parameters to initialize the private variables.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public Course(String courseName, String deptName, String courseID, int numberOfCredits, String description) {
        this.m_courseName = courseName;
        this.m_deptName = deptName;
        this.m_courseID = courseID;
        this.m_numberOfCredits = numberOfCredits;
        this.m_description = description;
    }
    
    /**/
    /*
    NAME :  getSectionCount
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_sectionCount.  
    RETURNS : int sectionCount.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public int getSectionCount() {
        return m_sectionCount;
    }

    /**/
    /*
    NAME :  setSectionCount
    SYNOPSIS : 
    	   int sectionCount  --> count of number of sections         
    DESCRIPTION :
			setter for m_sectionCount.  
    RETURNS : none
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public void setSectionCount(int sectionCount) {
        this.m_sectionCount = sectionCount;
    }

    /**/
    /*
    NAME :  getCourseName
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_courseName.  
    RETURNS : String m_courseName.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String getCourseName() {
        return m_courseName;
    }

    /**/
    /*
    NAME :  setCourseName
    SYNOPSIS : 
    	  String courseName   --> set the course name           
    DESCRIPTION :
			setter for m_courseName.  
    RETURNS : none
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public void setCourseName(String courseName) {
        this.m_courseName = courseName;
    }

    /**/
    /*
    NAME :  getDeptName
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_courseName.  
    RETURNS : String m_deptName.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String getDeptName() {
        return m_deptName;
    }

    /**/
    /*
    NAME :  setDeptName
    SYNOPSIS : 
    	  String deptName 	--> sets deptartment name            
    DESCRIPTION :
			setter for m_courseName.  
    RETURNS : none.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public void setDeptName(String deptName) {
        this.m_deptName = deptName;
    }

    /**/
    /*
    NAME :  getCourseId
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_courseID.  
    RETURNS : String m_courseId.
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String getCourseID() {
        return m_courseID;
    }

    /**/
    /*
    NAME :  setCourseId
    SYNOPSIS : 
    	  String courseId   --> sets courseID of the course           
    DESCRIPTION :
			setter for m_courseID.  
    RETURNS : none. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public void setCourseID(String courseID) {
        this.m_courseID = courseID;
    }

    /**/
    /*
    NAME :  getNumberOfCredits
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_numberOfCredits  
    RETURNS : int m_numberOfCredits
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public int getNumberOfCredits() {
        return m_numberOfCredits;
    }
    
    /**/
    /*
    NAME :  setNumberOfCredits
    SYNOPSIS : 
    	    int numberOfCredits  --> sets the number of credits         
    DESCRIPTION :
			setter for m_numberOfCredits  
    RETURNS : none
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public void setNumberOfCredits(int numberOfCredits) {
        this.m_numberOfCredits = numberOfCredits;
    }

    /**/
    /*
    NAME :  getDescription
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_description  
    RETURNS : String m_description
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public String getDescription() {
        return m_description;
    }

    /**/
    /*
    NAME :  setDescription
    SYNOPSIS : 
    	  String description --> sets the description of the course           
    DESCRIPTION :
			setter for m_description  
    RETURNS : none
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public void setDescription(String description) {
        this.m_description = description;

    }

    /**/
    /*
    NAME :  getTotalCost
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function returns the total cost of the course. i.e number of credits * cost of each credit.   
    RETURNS : double totalCost
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    public double getTotalCost() {
        return costPerCredit * this.m_numberOfCredits;
    }


    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the course into string, which is overriden to 
			be displayed on the screen too.    
    RETURNS : String 
    AUTHOR : Rijesh Kumar 
    DATE : 05/05/15 
    */
    /**/
    @Override
    public String toString() {
        return "Course{" +
                "courseName='" + m_courseName + '\'' +
                ", deptName='" + m_deptName + '\'' +
                ", courseID='" + m_courseID + '\'' +
                ", numberOfCredits=" + m_numberOfCredits +
                ", description='" + m_description + '\'' +
                ", totalCost= " + this.getTotalCost() +
                '}';
    }
}

