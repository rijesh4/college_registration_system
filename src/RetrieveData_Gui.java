/**/
/*
CLASS NAME :  RetrieveData_Gui 
         
DESCRIPTION :
		This class is responsible for creating screen that shows the result for the retrieval of data from the 
		file system. It handles proper access of those classes that have the permission to access the fileSystem. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RetrieveData_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;


    Container window;
    private JPanel m_retrievedataPanel;
    private JButton m_retrievedata2Button;
    private JLabel m_retrievedataconfirmLabel;
    private JButton m_retrievedatabackButton;

    /**/
    /*
    NAME : RetrieveData_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public RetrieveData_Gui(Container window) {

        currentStateNo = RET_DATA;
        this.window = window;
        m_retrievedataPanel = new JPanel();
        m_retrievedataPanel.setLayout(null);

        m_retrievedata2Button = new JButton("Retrieve data");
        m_retrievedata2Button.setBounds(10, 10, 150, 30);
        m_retrievedata2Button.addActionListener(this);
        m_retrievedataPanel.add(m_retrievedata2Button);

        m_retrievedataconfirmLabel = new JLabel("");
        m_retrievedataconfirmLabel.setBounds(10, 60, 250, 30);
        m_retrievedataPanel.add(m_retrievedataconfirmLabel);

        m_retrievedatabackButton = new JButton("Back");
        m_retrievedatabackButton.setBounds(10, 610, 150, 30);
        m_retrievedatabackButton.addActionListener(this);
        m_retrievedataPanel.add(m_retrievedatabackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void display() {
        window.add(m_retrievedataPanel);
        m_retrievedataPanel.setVisible(true);
        setTitle("College Registration System- Retrieve Data");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                 
    DESCRIPTION :
    		The function handles the onClick events for the Retrieve data screen. The following being the events:
    		retrieve data: calls the retrieveData method from the college instance that initiates loading of data 
    						from the database. 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_retrievedata2Button) ) {
            // Get the data from disk.
            try {
                College tempUniversity = College.retrieve();
                if (tempUniversity != null) {
                    m_retrievedataconfirmLabel.setText("Data has been retrieved from the disk.");

                    college = tempUniversity;
                } else {
                    m_retrievedataconfirmLabel.setText("Data has not been retrieved from the disk.");
                    college = College.instance();
                }
            } catch (Exception cnfe) {
                cnfe.printStackTrace();
            }
        }

        if (e.getSource().equals(m_retrievedatabackButton) ) {
            m_retrievedataPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}

