/**/
/*
CLASS NAME :  AddCourse_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allow adding a course to the system. The adding 
		of course is handled by the registrar gui. 
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AddCourse_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;
    private JPanel m_addCoursePanel;
    private JTextField m_courseNameTextField;
    private JLabel m_courseNameLabel;
    private JTextField m_deptNameTextField;
    private JLabel m_deptNameLabel;
    private JTextField m_courseIDTextField;
    private JLabel m_courseIDLabel;
    private JTextField m_numberOfCreditsTextField;
    private JLabel m_numberOfCreditsLabel;
    private JTextField m_descriptionTextField;
    private JLabel m_descriptionLabel;

    private JButton m_addCourseconfirmButton;
    private JTextArea m_resultTextArea;
    private JButton m_addCourseBackButton;

    /**/
    /*
    NAME : AddCourse_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public AddCourse_Gui(Container window) {

        currentStateNo = ADD_COURSE;
        this.window = window;
        m_addCoursePanel = new JPanel();
        m_addCoursePanel.setLayout(null);

        m_courseNameTextField = new JTextField();
        m_courseNameTextField.setBounds(10, 10, 150, 30);
        m_addCoursePanel.add(m_courseNameTextField);

        m_courseNameLabel = new JLabel("Course Name");
        m_courseNameLabel.setBounds(200, 10, 150, 30);
        m_addCoursePanel.add(m_courseNameLabel);

        m_deptNameTextField = new JTextField();
        m_deptNameTextField.setBounds(10, 60, 150, 30);
        m_addCoursePanel.add(m_deptNameTextField);

        m_deptNameLabel = new JLabel("Department Name");
        m_deptNameLabel.setBounds(200, 60, 150, 30);
        m_addCoursePanel.add(m_deptNameLabel);

        m_courseIDTextField = new JTextField();
        m_courseIDTextField.setBounds(10, 110, 150, 30);
        m_addCoursePanel.add(m_courseIDTextField);

        m_courseIDLabel = new JLabel("CourseID");
        m_courseIDLabel.setBounds(200, 110, 150, 30);
        m_addCoursePanel.add(m_courseIDLabel);

        m_numberOfCreditsTextField = new JTextField();
        m_numberOfCreditsTextField.setBounds(10, 160, 150, 30);
        m_addCoursePanel.add(m_numberOfCreditsTextField);

        m_numberOfCreditsLabel = new JLabel("numberOfCredits");
        m_numberOfCreditsLabel.setBounds(200, 160, 150, 30);
        m_addCoursePanel.add(m_numberOfCreditsLabel);

        m_descriptionTextField = new JTextField();
        m_descriptionTextField.setBounds(10, 210, 150, 30);
        m_addCoursePanel.add(m_descriptionTextField);

        m_descriptionLabel = new JLabel("Course Description");
        m_descriptionLabel.setBounds(200, 210, 150, 30);
        m_addCoursePanel.add(m_descriptionLabel);

        m_addCourseconfirmButton = new JButton("Add Course");
        m_addCourseconfirmButton.setBounds(10, 310, 150, 30);
        m_addCourseconfirmButton.addActionListener(this);
        m_addCoursePanel.add(m_addCourseconfirmButton);

        m_resultTextArea = new JTextArea("Results:");
        m_resultTextArea.setBounds(10, 360, 570, 150);
        m_resultTextArea.setEditable(false);
        m_addCoursePanel.add(m_resultTextArea);

        m_addCourseBackButton = new JButton("Back");
        m_addCourseBackButton.setBounds(10, 610, 150, 30);
        m_addCourseBackButton.addActionListener(this);
        m_addCoursePanel.add(m_addCourseBackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void display() {
        window.add(m_addCoursePanel);
        m_addCoursePanel.setVisible(true);
        setTitle("College Registration System - Add Course");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            	ActionEvent e 		--> onClick event 
            	String name   		--> the input val from the courseNameTextField
            	String dept   		--> the input val from the deptTextField
            	String CourseID 	--> the input val from the courseIdTextField
            	String temp     	--> the input val from the numOfCreditsTextField to be parsed into integer value
            	String description  --> the input val from the descriptionTextField
            	int credits         --> parsed int val from the temp variable.  
                String result 		--> string result of adding the course into college instance. Success/failure 
                    
    DESCRIPTION :
    		The function is responsible for parsing the user input values in the gui fields. 
    		It is also responsible for adding the input values into the college instance, 
    		and add a new course in the system. 
    		It also handles the back pressed button, and takes the user to the previous screen as 
    		handled by  method changeState of GUI class.   

    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_addCourseconfirmButton) ) {
            String name = m_courseNameTextField.getText();
            String deptName = m_deptNameTextField.getText();
            String courseID = m_courseIDTextField.getText();
            String temp = m_numberOfCreditsTextField.getText();
            String description = m_descriptionTextField.getText();

            int credits = Integer.parseInt(temp);

            String result = college.instance().addCourse(name, deptName, courseID, credits, description);

            m_resultTextArea.setText(result);
        }

        if (e.getSource().equals(m_addCourseBackButton) ) {
            m_addCoursePanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}

