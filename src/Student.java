/**/
/*
CLASS NAME :  STUDENT
         
DESCRIPTION :
		The class Student is responsible for holding data of a student. The class also 
		manages a student's transcript data and sections registered for using various methods.    
AUTHOR : Rijesh Kumar 
DATE : 05/04/15 
*/
/**/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Student implements Serializable {
    // Version ID checked during deserialization
    private static final long serialVersionUID = 001L;

    private int m_id;
    private String m_name;
    private String m_address;
    private String m_phone;
    private double m_amountDue;
    private boolean m_holds;
    private String m_password;
    
   	//maintains a list of transcript for ecah student
    private ArrayList<Transcript> transcript = new ArrayList<Transcript>();
    private static Transcript secondIt;

    /*default constructor
     */
    public Student() {
    }

    /**/
    /*
    NAME :  Student
    SYNOPSIS : 
    	String Name				--> name of student 
    	String address			--> address of student
    	String phone			--> phone number of student
    	double amountDue		--> current balance due
    	boolean holds			--> any holds on account
             
    DESCRIPTION :
			The Constructor is responsible for creating a student object using the given parameters to initialize the private variables.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Student(String name, String address, String phone, double amountDue, boolean holds) {
        this.m_id = StudentIdServer.instance().getId();
        this.m_name = name;
        this.m_address = address;
        this.m_phone = phone;
        this.m_amountDue = amountDue;
        this.m_holds = holds;
        this.m_password = name;


    }
    
    /**/
    /*
    NAME :  transcriptIterator
    SYNOPSIS : 
    	     
    DESCRIPTION :
			method that returns a list of elements in the transcript of a student.  
    RETURNS : Iterator<Transcript> iterator 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Iterator<Transcript> transcriptIterator() {
        Iterator<Transcript> iterator = transcript.iterator();
        return iterator;
    }

    /**/
    /*
    NAME :  addSectionToStudentTranscript
    SYNOPSIS : 
    	     String sectionID			--> sectionId to add to the transcript
    	     String location 			--> required info to add a section to a transcript
    	     String time				--> required info to add a section to a transcript
    	     String semester 			--> required info to add a section to a transcript
    	     String course				--> required info to add a section to a transcript
    	     String year 				--> required info to add a section to a transcript
    DESCRIPTION :
			method that adds a new section to a students transcript  
    RETURNS : true if the add was successful 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    boolean addSectionToStudentTranscript(String sectionID, String location, String time, String semester, String course, String year){
        secondIt = new Transcript(sectionID, location, time, semester,course, year);
        transcript.add(secondIt);
        return true;
    }

    /**/
    /*
    NAME :  removeSectionFromTranscript
    SYNOPSIS : 
    	     String sectionID			--> section to remove from the student database
    DESCRIPTION :
			method that removes a section from a students transcript  
    RETURNS : true if the remove was successful 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    boolean removeSectionFromTranscript(String sectionId) {
        for (Iterator<Transcript> iterator = transcript.iterator(); iterator.hasNext();) {
            Transcript firstIt = (Transcript) iterator.next();
            if (firstIt.getSectionID().equals(sectionId)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    /**/
    /*
    NAME :  setGrade
    SYNOPSIS : 
    	     String sectionID			--> sectionId to enter a grade for
    	     String grade 				--> Grade input data
    DESCRIPTION :
			method adds a grade for a student  
    RETURNS : true if the input of grade was successful else 
    			returns false. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    boolean setGrade(String sectionId, String grade) {
        for (Iterator<Transcript> iterator = transcript.iterator(); iterator.hasNext();) {
            secondIt = (Transcript) iterator.next();
            if (secondIt.getSectionID().equals(sectionId)) {
                secondIt.setGrade(grade);
                return true;
            }
        }

        return false;
    }

    /**/
    /*
    NAME :  updateAmountDue
    SYNOPSIS : 
    	    double fee 	--> update student billing info with current fee amount         
    DESCRIPTION :
			the method updates the amount due for the student
    RETURNS : updated double m_amountDue.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void updateAmountDue(double fee) {

        this.m_amountDue += fee;

    }
    
    /**/
    /*
    NAME :  getPassword
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_password  
    RETURNS : String m_password
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getPassword() {
        return m_password;
    }

    /**/
    /*
    NAME :  setPassword
    SYNOPSIS : 
    	      String password       
    DESCRIPTION :
			setter for m_password  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setPassword(String password) {
        this.m_password = password;
    }

    /**/
    /*
    NAME :  getName
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_name.  
    RETURNS : int m_name.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getName() {
        return m_name;
    }

    /**/
    /*
    NAME :  getId
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_id.  
    RETURNS : int m_id.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public int getId() {
        return m_id;
    }

    /**/
    /*
    NAME :  setPhone
    SYNOPSIS : 
    	    String phone	--> new phone number       
    DESCRIPTION :
			setter for m_phone.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setPhone(String phone) {
        this.m_phone = phone;
    }

    /**/
    /*
    NAME :  setAddress
    SYNOPSIS : 
    	    String address	--> new address       
    DESCRIPTION :
			setter for m_address.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setAddress(String address) {
        this.m_address = address;
    }

    /**/
    /*
    NAME :  getPhone
    SYNOPSIS : 
    	           
    DESCRIPTION :
			getter for m_phone.  
    RETURNS :  String m_phone
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getPhone() {
        return m_phone;
    }

    /**/
    /*
    NAME :  getAddress
    SYNOPSIS : 
    	           
    DESCRIPTION :
			getter for m_address.  
    RETURNS :	String m_address 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getAddress() {

        return m_address;
    }

    /**/
    /*
    NAME :  getAmountDue
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_amountDue.  
    RETURNS : double m_amountDue.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public double getAmountDue() {
        return m_amountDue;

    }

    /**/
    /*
    NAME :  setAmountDue
    SYNOPSIS : 
    	      double amountDue       
    DESCRIPTION :
			setter for m_amountDue.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setAmountDue(double amountDue) {
        this.m_amountDue = amountDue;
    }

    /**/
    /*
    NAME :  setId
    SYNOPSIS : 
    	      int id.       
    DESCRIPTION :
			setter for m_id.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setId(int id) {
        this.m_id = id;
    }

    /**/
    /*
    NAME :  setName
    SYNOPSIS : 
    	     String name.        
    DESCRIPTION :
			setter for m_name.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setName(String name) {
        this.m_name = name;
    }

    /**/
    /*
    NAME :  isHolds
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter m_holds.  
    RETURNS : boolean m_holds.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean isHolds() {
        return m_holds;
    }

    /**/
    /*
    NAME :  setHolds
    SYNOPSIS : 
    	     boolean holds.        
    DESCRIPTION :
			setter m_holds.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setHolds(boolean holds) {
        this.m_holds = holds;
    }
    
    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the Student into string, which is overriden to 
			be displayed on the screen too.    
    RETURNS : String 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String toString() {
        return "id = " + m_id + '\n' +
                "name = " + m_name + '\n' +
                "address = " + m_address + '\n' +
                "phone = " + m_phone + '\n';
    }

}



