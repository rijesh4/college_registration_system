/**/
/*
CLASS NAME :  Drop_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allow removing a section from a students account. The adding 
		of section is handled by the registrar/ clerk/ Student . 
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Drop_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_dropPanel;
    private JTextField m_resultTextField;

    private JTextField m_sectionIDTextField;
    private JLabel m_sectionIDLabel;
    private JButton m_dropButton;
    private JButton m_dropBackButton;

//	 private JTextField result;

    /**/
    /*
    NAME : Drop_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public Drop_Gui(Container window) {

        currentStateNo = DROP;
        this.window = window;
        m_dropPanel = new JPanel();
        m_dropPanel.setLayout(null);

        m_sectionIDTextField = new JTextField();
        m_sectionIDTextField.setBounds(10, 110, 150, 30);
        m_dropPanel.add(m_sectionIDTextField);

        m_sectionIDLabel = new JLabel("Section ID");
        m_sectionIDLabel.setBounds(200, 110, 150, 30);
        m_dropPanel.add(m_sectionIDLabel);

        m_dropButton = new JButton("Drop");
        m_dropButton.setBounds(10, 160, 150, 30);
        m_dropButton.addActionListener(this);
        m_dropPanel.add(m_dropButton);

        m_resultTextField = new JTextField();
        m_resultTextField.setBounds(10, 220, 300, 300);
        m_dropPanel.add(m_resultTextField);

        m_dropBackButton = new JButton("Back");
        m_dropBackButton.setBounds(10, 610, 150, 30);
        m_dropBackButton.addActionListener(this);
        m_dropPanel.add(m_dropBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/  
    public void display() {
        window.add(m_dropPanel);
        m_dropPanel.setVisible(true);
        setTitle("College Registration System - Drop from a Section");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
    		String sectionId  		--> input value from the sectionIdTextField
    		String message 			--> message to be displayed. 
            	      	    
    DESCRIPTION :
    		The function handles the onClick events for the student Drop Menu screen. The following being the events:
    		dropButton: The event drops a course for a student. Student and Clerk both can drop a course for a 
    			student.
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/      
    public void actionPerformed(ActionEvent e) {


        if (e.getSource().equals(m_dropButton) ) {
            //DROP THE CLASS AND DISPLAY RESULTS
            String sectionID = m_sectionIDTextField.getText();

            int result = college.dropFromSection(STUDENT_ID, sectionID);

            String message = "";

            if (result == 0)
                message += "You have successfully dropped the section.";
            else if (result == 6)
                message += "The section could NOT be removed from the student's transcript.";
            else if (result == 7)
                message += "The student could NOT be removed from the section roster.";
            else
                message += "You were not able to drop the section because of an unknown error.";

            m_resultTextField.setText(message);
        }

        if (e.getSource().equals(m_dropBackButton) ) {
            m_dropPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
