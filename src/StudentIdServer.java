/**/
/*
CLASS NAME : STUDENTIDSERVER
         
DESCRIPTION :
		This class's primary role is to allocate a unique ID for a  student.   
AUTHOR : Rijesh Kumar 
DATE : 05/10/15 
*/
/**/

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class StudentIdServer implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4891429772919027937L;
	private int m_idCounter;
    private static StudentIdServer server;

    /**/
    /*
    NAME : StudentIdServer
    SYNOPSIS : 
    	     
    DESCRIPTION :
			constructor that makes sure there is only one instance of each private variable  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    private StudentIdServer() {
        m_idCounter = 1;
    }

    /**/
    /*
    NAME :  StudentIdServer instance()
    SYNOPSIS : 
    	     
    DESCRIPTION :
			constructor that makes sure there is only one instance of StudentIdServer  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    public static StudentIdServer instance() {
        if (server == null) {
            return (server = new StudentIdServer());
        } else {
            return server;
        }
    }

    /**/
    /*
    NAME :  retrieve
    SYNOPSIS : 
    	     ObjectInputStream input	--> the read object 
    DESCRIPTION :
			method that retrieves saved data for the serverId to keep count of the current Id of the student.     
    RETURNS : void
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    public static void retrieve(ObjectInputStream input) {
        try {
            server = (StudentIdServer) input.readObject();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception cnfe) {
            cnfe.printStackTrace();
        }
    }

    /**/
    /*
    NAME :  writeObject
    SYNOPSIS :   
    	     ObjectOutputStream output	--> the object as passed in as parameter by the save method to write to the file. 
    DESCRIPTION :
			method that writes out to a file all the studentId server data.     
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    private void writeObject(java.io.ObjectOutputStream output) throws IOException {
        try {
            output.defaultWriteObject();
            output.writeObject(server);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**/
    /*
    NAME :  readObject
    SYNOPSIS :   
    	     ObjectInputStream input	--> the read object as passed in as parameter by the retrieve method. 
    DESCRIPTION :
			method that that reads in all serverId data from a file    
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    private void readObject(java.io.ObjectInputStream input) throws IOException, ClassNotFoundException {
        try {
            input.defaultReadObject();
            if (server == null) {
                server = (StudentIdServer) input.readObject();
            } else {
                input.readObject();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    /**/
    /*
    NAME :  getId
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_idCounter.
    RETURNS : int m_idCounter. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    public int getId() {
        return m_idCounter++;
    }

    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the studentIdServer into string.
    RETURNS : String value of the studentIdServer. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/10/15 
    */
    /**/
    public String toString() {
        return ("IdServer" + m_idCounter);
    }
}



