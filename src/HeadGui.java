/**/
/*
CLASS NAME :  HeadGui
         
DESCRIPTION :
		The HeadGui is basically an abstract class, where the static values are used to create a screen of any sort as long as some basic 
		methods are implemented, and the transition is valid. For example. Code Login_panel is used to create the Gui_login. It as any other screen
		must only implement the display, actionPerformed methods only. This type of design pattern creates a loosely connected design, helping in
		flexibility of the program. 
AUTHOR : Rijesh Kumar 
DATE :  04/25/15  
*/
/**/

import javax.swing.JFrame;

public abstract class HeadGui extends JFrame {

    private static final long serialVersionUID = 1L;
    protected static final int MAIN_PANEL = 0;
    protected static final int LOGIN_PANEL = 1;
    protected static final int REGISTRAR_PANEL = 2;
    protected static final int CLERK_PANEL = 3;
    protected static final int STUDENT_PANEL = 4;
    protected static final int ADD_STUDENT = 5;
    protected static final int ADD_COURSE = 6;
    protected static final int ADD_SECTION = 7;
    protected static final int SECTION_ROSTER = 8;
    protected static final int COURSE_LIST = 9;
    protected static final int SECTION_LIST = 10;
    protected static final int SAVE_DATA = 11;
    protected static final int RET_DATA = 12;
    protected static final int CLASS_LIST = 13;
    protected static final int EDIT_GRADE = 14;
    protected static final int TRANSCRIPT = 15;
    protected static final int REGISTER = 16;
    protected static final int DROP = 17;
    protected static final int ACCOUNT = 18;
    protected static final int CHECKGRADE = 19;
    protected static final int CLERK_AS_STUDENT = 20;
    protected static final int CLASS_SCHEDULE = 21;
    protected static final int MASS_EDIT_GRADE = 22;
    protected static final int GENERATE_TUITION = 23;
    protected static final int EDIT_GRADES_SECTION = 24;

    protected static int currentStateNo;
    protected static int exitCode;
    protected static int STUDENT_ID = 0;
    protected static String SECTION_ID = "";

    protected static College college;
}

