/**/
/*
CLASS NAME :  ViewSectionRoster_Gui
         
DESCRIPTION :
		This class is responsible for creating screen where all the students in a particular section can be viewed and 
		some common change be made to each accordingly, by use of other method which can be called upon by transitioning of the screen .  
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ViewSectionRoster_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_sectionRosterPanel;
    private JLabel m_sectionIDLabel;
    private JTextField m_sectionIDTextField;
    private JButton m_viewSectionRosterButton;
    private JButton m_sectionRosterBackButton;
    private JTextArea m_resultTextArea;
    private JLabel m_output;

    //private JTextField result;

    /**/
    /*
    NAME : ViewSectionRoster_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/ 
    public ViewSectionRoster_Gui(Container window) {

        currentStateNo = SECTION_ROSTER;
        this.window = window;
        m_sectionRosterPanel = new JPanel();
        m_sectionRosterPanel.setLayout(null);

        m_sectionIDTextField = new JTextField();
        m_sectionIDTextField.setBounds(10, 60, 150, 30);
        m_sectionRosterPanel.add(m_sectionIDTextField);

        m_sectionIDLabel = new JLabel("Section ID");
        m_sectionIDLabel.setBounds(200, 60, 150, 30);
        m_sectionRosterPanel.add(m_sectionIDLabel);

        m_viewSectionRosterButton = new JButton("View Section Roster");
        m_viewSectionRosterButton.setBounds(10, 110, 150, 30);
        m_viewSectionRosterButton.addActionListener(this);
        m_sectionRosterPanel.add(m_viewSectionRosterButton);

        m_output = new JLabel ("Result");
        m_output.setBounds(10,160,150,30);
        m_sectionRosterPanel.add(m_output);

         m_resultTextArea = new JTextArea("\n");
        m_resultTextArea.setBounds(10, 220, 570, 500);
        m_resultTextArea.setEditable(false);
        m_sectionRosterPanel.add(m_resultTextArea);

        /*result = new JTextField();
        result.setBounds(10, 220, 600, 600);
        sectionRosterPanel.add(result);        */

        m_sectionRosterBackButton = new JButton("Back");
        m_sectionRosterBackButton.setBounds(600, 10, 150, 30);
        m_sectionRosterBackButton.addActionListener(this);
        m_sectionRosterPanel.add(m_sectionRosterBackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/ 
    public void display() {
        window.add(m_sectionRosterPanel);
        m_sectionRosterPanel.setVisible(true);
        setTitle("College Registration System - View Section Roster");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
    		Section result 						--> stores the section object returned from the getSection(sectionID) method from
    												the college instance, using the sectionID from the sectionIDTextField. 
    		String message						--> the output message showing success/failure to the result
    		Iterator<SectionRoster> iterator	--> iterator if SectionRoster that iterates through each instace of the section 
    		
                 
    DESCRIPTION :
    		The function handles the onClick events for the view section roster screen. The following being the events:
    		ViewSectionRoster: shows the info about a given section from the database.  
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/ 
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals( m_viewSectionRosterButton)) {
            String sectionID = m_sectionIDTextField.getText();


            Section result = college.instance().getSection(sectionID);

            String message = "";

            if (result == null) {
                message += "That section does NOT exist.";
                m_resultTextArea.setText(message);
            } else
            {

            Iterator<SectionRoster> iterator = college.getSectionRosterIterator(sectionID);

            while (iterator.hasNext())    //while there are still more students
            {
                SectionRoster tempSectionRoster = (SectionRoster) iterator.next();
               // roster += tempSectionRoster.getStudentID() + "	" + tempSectionRoster.getName();
                  m_resultTextArea.append(tempSectionRoster.getStudentID()+ "." +tempSectionRoster.getName () +"\n");
            }
           // resultTextArea.append(roster);
            }

        }

        if (e.getSource().equals(m_sectionRosterBackButton) ) {
            m_sectionRosterPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
