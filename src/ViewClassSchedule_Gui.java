/**/
/*
CLASS NAME :  ViewAccount_Gui
         
DESCRIPTION :
		This class is responsible for creating screen where a student can view his/her class Schedule details.  
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ViewClassSchedule_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_classSchedulePanel;
    private JLabel m_titleLabel;
    private JLabel m_heading1Label;
    private JLabel m_heading2Label;
    private JLabel m_classLabel;
    private JButton m_classScheduleBackButton;

    /**/
    /*
    NAME : ViewClassSchedule_Gui 
    SYNOPSIS :
             Container window   			--> window to which the GUI elements need to be added. 
             Iterator<Transcript> iterator  --> transcript iterator to iterate through each instance of the
             									transcript and print the data to the screen. 
             
             
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None. 
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public ViewClassSchedule_Gui(Container window) {

        currentStateNo = CLASS_SCHEDULE;
        this.window = window;
        m_classSchedulePanel = new JPanel();
        m_classSchedulePanel.setLayout(null);

        String title = "CLASS SCHEDULE";
        String heading1 = "Course               Section ID          Sem             Year          location" + "          " + "Time";
        String heading2 = "-------------------------------------------------------------------------------------------------------";

        m_titleLabel = new JLabel(title);
        m_titleLabel.setBounds(10, 10, 250, 30);
        m_classSchedulePanel.add(m_titleLabel);

        m_heading1Label = new JLabel(heading1);
        m_heading1Label.setBounds(10, 60, 700, 30);
        m_classSchedulePanel.add(m_heading1Label);

        m_heading2Label = new JLabel(heading2);
        m_heading2Label.setBounds(10, 80, 700, 30);
        m_classSchedulePanel.add(m_heading2Label);

        int x = 80;
        Iterator<Transcript> iterator = college.getTranscriptIterator(STUDENT_ID);

        while (iterator.hasNext())    //while there are still more students
        {
            String classes = "";
            Transcript tempTranscript = (Transcript) iterator.next();

            if (tempTranscript.getGrade().equals("Z")) {
                classes += tempTranscript.getCourse() + "               " + tempTranscript.getSectionID() + "          " +
                        tempTranscript.getSemester() + "                  " +
                        tempTranscript.getYear() + "          " + tempTranscript.getLocation() +
                        "               " + tempTranscript.getTime() + '\n';

                m_classLabel = new JLabel(classes);
                m_classLabel.setBounds(10, x += 20, 700, 30);
                m_classSchedulePanel.add(m_classLabel);

            }
        }


        m_classScheduleBackButton = new JButton("Back");
        m_classScheduleBackButton.setBounds(10, 610, 150, 30);
        m_classScheduleBackButton.addActionListener(this);
        m_classSchedulePanel.add(m_classScheduleBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_classSchedulePanel);
        m_classSchedulePanel.setVisible(true);
        setTitle("College Registration System - View Class Schedule");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                 
    DESCRIPTION :
    		The function handles the onClick events for the View Schedule screen to 
    		displays the student's  currently taken course info in the base screen. The event also handles the backButton which
    		takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(m_classScheduleBackButton) ) {
            m_classSchedulePanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
