/**/
/*
CLASS NAME :  GenerateTuitionBill_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to get the tuition bill information for the currently enrolled
		students. The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GenerateTuitionBill_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_transcriptPanel;
    private JLabel m_titleLabel;
    private JLabel m_headingLabel;
    private JLabel m_studentLabel;
    private JButton m_transcriptBackButton;

    /**/
    /*
    NAME : GenerateTuitionBill_Gui
    SYNOPSIS :
            Container window   						--> window to which the GUI elements need to be added. 
            Iterator<Student> iterator 				--> Student iterator
            String student 						    --> stores student's name, id, address and amount due to be displayed
            
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public GenerateTuitionBill_Gui(Container window) {

        currentStateNo = TRANSCRIPT;
        this.window = window;
        m_transcriptPanel = new JPanel();
        m_transcriptPanel.setLayout(null);

        String title = "TUITION BILLS";
        String heading = "--------------------------------------------------------------------------------------------";

        m_titleLabel = new JLabel(title);
        m_titleLabel.setBounds(10, 10, 250, 30);
        m_transcriptPanel.add(m_titleLabel);

        m_headingLabel = new JLabel(heading);
        m_headingLabel.setBounds(10, 60, 700, 30);
        m_transcriptPanel.add(m_headingLabel);

        int x = 80;
        Iterator<Student> iterator = college.getStudentListIterator();

        while (iterator.hasNext())    //while there are still more students
        {
            String student = "";
            Student tempStudent = (Student) iterator.next();

            if ((tempStudent.getAmountDue()) > 0) {
                student += tempStudent.getId() + "." + tempStudent.getName() +
                        "   " +
                        tempStudent.getAddress() + "  " + tempStudent.getAmountDue();

                m_studentLabel = new JLabel(student);
                m_studentLabel.setBounds(10, x += 20, 700, 30);
                m_transcriptPanel.add(m_studentLabel);
            }
        }

        m_transcriptBackButton = new JButton("Back");
        m_transcriptBackButton.setBounds(10, 610, 150, 30);
        m_transcriptBackButton.addActionListener(this);
        m_transcriptPanel.add(m_transcriptBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_transcriptPanel);
        m_transcriptPanel.setVisible(true);
        setTitle("University System - View Transcript");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                	    
    DESCRIPTION :
    		The function handles the onClick events for the Generate Tution screen. The following being the events: 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(m_transcriptBackButton) ) {
            m_transcriptPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
