/**/
/*
CLASS NAME :  SECTIONROSTER
         
DESCRIPTION :
		This class helps in object oriented organization of the sections so as to make it 
		easier/flexibly accessible.     
AUTHOR : Rijesh Kumar 
DATE : 05/04/15 
*/
/**/

import java.io.Serializable;

public class SectionRoster implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int m_studentID;
    private String name;
    
    /**/
    /*
    NAME :  SectionRoster
    SYNOPSIS : 
    	int studentId		--> student Id 
    	String name 		--> name
    	     
    DESCRIPTION :
			The Constructor is responsible for initializing Section Roster object.   
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public SectionRoster(int studentID, String name) {
        this.m_studentID = studentID;
        this.name = name;
    }

    /**/
    /*
    NAME :  getId
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_id.  
    RETURNS : int m_id.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public int getStudentID() {
        return m_studentID;
    }

    /**/
    /*
    NAME :  setId
    SYNOPSIS : 
    	      int id.       
    DESCRIPTION :
			setter for m_id.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setStudentID(int studentID) {
        this.m_studentID = studentID;
    }

    /**/
    /*
    NAME :  getName
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_name.  
    RETURNS : int m_name.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getName() {
        return name;
    }

    /**/
    /*
    NAME :  setName
    SYNOPSIS : 
    	     String name.        
    DESCRIPTION :
			setter for m_name.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setName(String name) {
        this.name = name;
    }

    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the section Roster into string, which is overriden to 
			be displayed on the screen too.    
    RETURNS : String 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    @Override
    public String toString() {
        return "SectionRoster{" +
                "studentID=" + m_studentID +
                ", name='" + name + '\'' +
                '}';
    }
}
