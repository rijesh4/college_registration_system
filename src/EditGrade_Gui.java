/**/
/*
CLASS NAME :  EditGrade_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allow editing a specific student's grade for a specific section. This class 
		works as a helper class for the EditSectionGrade gui . The adding 
		of section is handled by the registrar/clerk. Mass editing is done using another gui class (MassEditGrade).
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
//import java.lang.Integer;

public class EditGrade_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_editGradePanel;
    private JTextField m_studentIDTextField;
    private JLabel m_studentIDLabel;
    private JTextField m_sectionIDTextField;
    private JLabel m_sectionIDLabel;
    private JTextField m_gradeTextField;
    private JLabel m_gradeLabel;
    private JButton m_enterGradeButton;
    private JButton m_editGradeBackButton;
    private JTextField m_result;

    /**/
    /*
    NAME : EditGrade_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/         
    public EditGrade_Gui(Container window) {

        currentStateNo = EDIT_GRADE;
        this.window = window;
        m_editGradePanel = new JPanel();
        m_editGradePanel.setLayout(null);

        m_studentIDTextField = new JTextField();
        m_studentIDTextField.setBounds(10, 60, 150, 30);
        m_editGradePanel.add(m_studentIDTextField);

        m_studentIDLabel = new JLabel("Student ID");
        m_studentIDLabel.setBounds(200, 60, 150, 30);
        m_editGradePanel.add(m_studentIDLabel);

        m_sectionIDTextField = new JTextField();
        m_sectionIDTextField.setBounds(10, 110, 150, 30);
        m_editGradePanel.add(m_sectionIDTextField);

        m_sectionIDLabel = new JLabel("Section ID");
        m_sectionIDLabel.setBounds(200, 110, 150, 30);
        m_editGradePanel.add(m_sectionIDLabel);

        m_gradeTextField = new JTextField();
        m_gradeTextField.setBounds(10, 160, 150, 30);
        m_editGradePanel.add(m_gradeTextField);

        m_gradeLabel = new JLabel("Grade");
        m_gradeLabel.setBounds(200, 160, 150, 30);
        m_editGradePanel.add(m_gradeLabel);

        m_enterGradeButton = new JButton("Enter Grade");
        m_enterGradeButton.setBounds(10, 210, 150, 30);
        m_enterGradeButton.addActionListener(this);
        m_editGradePanel.add(m_enterGradeButton);

        m_result = new JTextField();
        m_result.setBounds(10, 300, 250, 250);
//        result.setEditable(false);
        m_editGradePanel.add(m_result);

        m_editGradeBackButton = new JButton("Back");
        m_editGradeBackButton.setBounds(10, 610, 150, 30);
        m_editGradeBackButton.addActionListener(this);
        m_editGradePanel.add(m_editGradeBackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/      
    public void display() {
        setTitle("College Registration System - Edit Grade");
        window.add(m_editGradePanel);
        m_editGradePanel.setVisible(true);
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            String sectionId  		--> input value from the sectionIdTextField
    		String grade 			--> message to be displayed.       	    
    DESCRIPTION :
    		The function handles the onClick events for the Edit Grade screen. The following being the events:
    		enterGradeButton: The event calls the studentGrade method with the studentId and SectionId to 
    		confirm the entered grade. It accordingly sets the message to the textField. 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_enterGradeButton) ) {

            int studentID = Integer.parseInt(m_studentIDTextField.getText());
            String sectionID = m_sectionIDTextField.getText();
            String grade = m_gradeTextField.getText();


            if (college.studentGrade(studentID, sectionID, grade) == 0) {
                m_result.setText("Grade Entered. ");
            } else if (college.studentGrade(studentID, sectionID, grade) == 1) {
                m_result.setText("Student does not exist. ");
            } else if (college.studentGrade(studentID, sectionID, grade) == 2) {
                m_result.setText("Section does not exist. ");
            } else if (college.studentGrade(studentID, sectionID, grade) == 8) {
                m_result.setText("Grade Not Entered. ");
            }
        }

        if (e.getSource().equals(m_editGradeBackButton) ) {
            m_editGradePanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}
