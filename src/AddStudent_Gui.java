/**/
/*
CLASS NAME :  AddStudent_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allow adding a student to the system. The adding 
		of student is handled by the clerk gui. 
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AddStudent_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;
    private JPanel m_addStudentPanel;
    private JTextField m_nameTextField;
    private JLabel m_nameLabel;
    private JTextField m_holdsTextField;
    private JLabel m_holdsLabel;
    private JTextField m_addressTextField;
    private JLabel m_addressLabel;
    private JTextField m_phoneTextField;
    private JLabel m_phoneLabel;
    private JButton m_addStudentconfirmButton;
    private JTextArea m_resultTextArea;
    private JButton m_addStudentBackButton;

    /**/
    /*
    NAME : AddStudent_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public AddStudent_Gui(Container window) {

        currentStateNo = ADD_STUDENT;
        this.window = window;
        m_addStudentPanel = new JPanel();
        m_addStudentPanel.setLayout(null);

        m_nameTextField = new JTextField();
        m_nameTextField.setBounds(10, 10, 150, 30);
        m_addStudentPanel.add(m_nameTextField);

        m_nameLabel = new JLabel("Name");
        m_nameLabel.setBounds(200, 10, 150, 30);
        m_addStudentPanel.add(m_nameLabel);

        m_holdsTextField = new JTextField();
        m_holdsTextField.setBounds(10, 60, 150, 30);
        m_addStudentPanel.add(m_holdsTextField);

        m_holdsLabel = new JLabel("Holds");
        m_holdsLabel.setBounds(200, 60, 150, 30);
        m_addStudentPanel.add(m_holdsLabel);


        m_addressTextField = new JTextField();
        m_addressTextField.setBounds(10, 210, 550, 30);
        m_addStudentPanel.add(m_addressTextField);

        m_addressLabel = new JLabel("Address");
        m_addressLabel.setBounds(10, 180, 150, 30);
        m_addStudentPanel.add(m_addressLabel);

        m_phoneTextField = new JTextField();
        m_phoneTextField.setBounds(10, 260, 150, 30);
        m_addStudentPanel.add(m_phoneTextField);

        m_phoneLabel = new JLabel("Phone");
        m_phoneLabel.setBounds(200, 260, 150, 30);
        m_addStudentPanel.add(m_phoneLabel);

        m_addStudentconfirmButton = new JButton("Add Student");
        m_addStudentconfirmButton.setBounds(10, 310, 150, 30);
        m_addStudentconfirmButton.addActionListener(this);
        m_addStudentPanel.add(m_addStudentconfirmButton);

        m_resultTextArea = new JTextArea("Results:");
        m_resultTextArea.setBounds(10, 360, 570, 150);
        m_resultTextArea.setEditable(false);
        m_addStudentPanel.add(m_resultTextArea);

        m_addStudentBackButton = new JButton("Back");
        m_addStudentBackButton.setBounds(10, 610, 150, 30);
        m_addStudentBackButton.addActionListener(this);
        m_addStudentPanel.add(m_addStudentBackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_addStudentPanel);
        m_addStudentPanel.setVisible(true);
        setTitle("College Registration System - Add Student");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            	ActionEvent e 		--> onClick event 
            	String name   		--> the input val from the nameTextField
            	String response   	--> the input val from the holdsTextField
            	String address 	--> the input val from the addressTextField
            	String phone     --> the input val from the phoneTextField to be parsed into integer value
            	boolean holds  		--> stores the value of the holds from the response input.   
                String result 		--> string result of adding the course into college instance. Success/failure 
                    
    DESCRIPTION :
    		The function is responsible for parsing the user input values in the gui fields. 
    		It is also responsible for adding the input values into the college instance, 
    		and add a new student in the system. 
    		It also handles the back pressed button, and takes the user to the previous screen as 
    		handled by  method changeState of GUI class.   

    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_addStudentconfirmButton) ) {
            String name = m_nameTextField.getText();
            String response = m_holdsTextField.getText();
            String address = m_addressTextField.getText();
            String phone = m_phoneTextField.getText();
            boolean holds = false;

            if (response.equals("yes") || response.equals("y")) {
                holds = true;
            }

            String result = college.instance().addStudent(name, address, phone, holds);
            m_resultTextArea.setText(result);
        }

        if (e.getSource().equals(m_addStudentBackButton) ) {
            m_addStudentPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}

