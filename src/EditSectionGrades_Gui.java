/**/
/*
CLASS NAME :  EditSectionGrades_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allow editing a grade for a specific section. It is different 
		from the EditGrade_gui as it can change grade for multiple students in a section. This can be thought of 
		as cuving of a grade. This class is also used as a helper function for the MassEditGrade. The adding 
		of section is handled by the registrar/clerk. Mass editing is done using another gui class (MassEditGrade).
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EditSectionGrades_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_sectionGradesPanel;
    private JLabel m_studentLabel;
    private JTextField m_resultTextField;
    private JLabel m_titleLabel;
    private JLabel m_headingLabel;
    private JButton m_enterGradeButton;
    private JLabel m_gradeLabel;
    private JTextField m_gradeTextField;
    private JLabel m_enterStudentIDLabel;
    private JTextField m_enterStudentIDTextField;
    private JButton m_sectionGradesBackButton;

    /**/
    /*
    NAME : EditSectionGrades_Gui 
    SYNOPSIS :
            Container window   						--> window to which the GUI elements need to be added. 
            int xBound			   					--> sets a bound for labels created   
            Iterator<SectionRoster> iterator1		--> SectionRoster iterator
            Iterator<Transcript> iterator2			--> Transcript iterator 
            String student 						    --> stores student's name, id and grade to be displayed
            
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public EditSectionGrades_Gui(Container window) {

        currentStateNo = EDIT_GRADES_SECTION;
        this.window = window;
        m_sectionGradesPanel = new JPanel();
        m_sectionGradesPanel.setLayout(null);

        m_titleLabel = new JLabel("STUDENTS IN SECTION");
        m_titleLabel.setBounds(10, 10, 700, 30);
        m_sectionGradesPanel.add(m_titleLabel);

        Iterator<SectionRoster> iterator1 = college.getSectionRosterIterator(SECTION_ID);
        int xBound= 30;

        while (iterator1.hasNext()) {
            String student = "";
            SectionRoster tempSectionRoster = (SectionRoster) iterator1.next();

            student += tempSectionRoster.getStudentID() + "." + tempSectionRoster.getName() + " ";
            Iterator<Transcript> iterator2 = college.getTranscriptIterator(tempSectionRoster.getStudentID());
            //Iterator<Transcript> iterator = college.getTranscriptIterator(STUDENT_ID);
            while (iterator2.hasNext()) {
                Transcript tempTranscript = (Transcript) iterator2.next();
                if (tempTranscript.getSectionID().equals(SECTION_ID)) {
                    student += tempTranscript.getGrade();
                   // grade = tempTranscript.getGrade ();
                    break;
                }
            }

            m_studentLabel = new JLabel(student);
            m_studentLabel.setBounds(10, xBound+= 20, 700, 30);
            m_sectionGradesPanel.add(m_studentLabel);
        }

        m_headingLabel = new JLabel("ENTER A STUDENT ID AND GRADE");
        m_headingLabel.setBounds(10, xBound+= 40, 700, 30);
        m_sectionGradesPanel.add(m_headingLabel);

        m_enterStudentIDTextField = new JTextField();
        m_enterStudentIDTextField.setBounds(10, xBound+= 40, 150, 30);
        m_sectionGradesPanel.add(m_enterStudentIDTextField);

        m_enterStudentIDLabel = new JLabel("Student ID");
        m_enterStudentIDLabel.setBounds(200, xBound, 150, 30);
        m_sectionGradesPanel.add(m_enterStudentIDLabel);

        m_gradeTextField = new JTextField();
        m_gradeTextField.setBounds(10, xBound+= 40, 150, 30);
        m_sectionGradesPanel.add(m_gradeTextField);

        m_gradeLabel = new JLabel("Grade");
        m_gradeLabel.setBounds(200, xBound, 150, 30);
        m_sectionGradesPanel.add(m_gradeLabel);

        m_enterGradeButton = new JButton("Enter Grade");
        m_enterGradeButton.setBounds(10, xBound+= 40, 150, 30);
        m_enterGradeButton.addActionListener(this);
        m_sectionGradesPanel.add(m_enterGradeButton);

        m_resultTextField = new JTextField();
        m_resultTextField.setBounds(10, xBound+= 60, 250, 100);
        m_sectionGradesPanel.add(m_resultTextField);

        m_sectionGradesBackButton = new JButton("Back");
        m_sectionGradesBackButton.setBounds(600, 10, 150, 30);
        m_sectionGradesBackButton.addActionListener(this);
        m_sectionGradesPanel.add(m_sectionGradesBackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_sectionGradesPanel);
        m_sectionGradesPanel.setVisible(true);
        setTitle("College Registration System - Enter Grades For A Section");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            String sectionId  		--> input value from the sectionIdTextField
    		String grade 			--> message to be displayed.       	    
    DESCRIPTION :
    		The function handles the onClick events for the Edit Section Grade screen. The following being the events:
    		enterGradeButton: The event calls the studentGrade method with the studentId and SectionId to 
    		confirm the entered grade. It accordingly sets the message to the textField. 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_enterGradeButton) ) {
            int studentID = Integer.parseInt(m_enterStudentIDTextField.getText());
            String grade = m_gradeTextField.getText();

            if (college.studentGrade(studentID, SECTION_ID, grade) == 8) {
                m_resultTextField.setText("ERROR : Grade Not Entered. ");
            }
            m_sectionGradesPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, EDIT_GRADES_SECTION);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_sectionGradesBackButton) ) {
            m_sectionGradesPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, MASS_EDIT_GRADE);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}
