/**/
/*
CLASS NAME :  SaveData_Gui 
         
DESCRIPTION :
		This class is responsible for creating screen that shows the result for the saving of data to the 
		file system. It handles proper access of those classes that have the permission to access the fileSystem. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SaveData_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;
    private JPanel m_savedataPanel;
    private JButton m_savedata2Button;
    private JLabel m_savedataconfirmLabel;
    private JButton m_savedatabackButton;

    /**/
    /*
    NAME : SaveData_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public SaveData_Gui(Container window) {

        currentStateNo = SAVE_DATA;
        this.window = window;
        m_savedataPanel = new JPanel();
        m_savedataPanel.setLayout(null);

        m_savedata2Button = new JButton("Save data");
        m_savedata2Button.setBounds(10, 10, 150, 30);
        m_savedata2Button.addActionListener(this);
        m_savedataPanel.add(m_savedata2Button);

        m_savedataconfirmLabel = new JLabel("");
        m_savedataconfirmLabel.setBounds(10, 60, 250, 30);
        m_savedataPanel.add(m_savedataconfirmLabel);

        m_savedatabackButton = new JButton("Back");
        m_savedatabackButton.setBounds(10, 610, 150, 30);
        m_savedatabackButton.addActionListener(this);
        m_savedataPanel.add(m_savedatabackButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void display() {
        window.add(m_savedataPanel);
        m_savedataPanel.setVisible(true);
        setTitle("College Registration System- Save Data");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                 
    DESCRIPTION :
    		The function handles the onClick events for the Save data screen. The following being the events:
    		save data: calls the saveData method from the college instance that initiates saving of data 
    						to the database. 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(m_savedata2Button) ) {
            boolean flag;
            flag = college.instance().save();

            if (flag)
                m_savedataconfirmLabel.setText("Data has been saved to disk.");

            else {
                m_savedataconfirmLabel.setText("Error saving data.");
            }
        }

        if (e.getSource().equals(m_savedatabackButton) ) {
            m_savedataPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}



