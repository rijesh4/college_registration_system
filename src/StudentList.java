/**/
/*
CLASS NAME :  STUDENTLIST
         
DESCRIPTION :
		The class StudentList is responsible for holding data of List of all students. 
		The List is statically available to be used by the singular college object. It also 
		has access to the file system to update the info from the file sytem, and manipulate it 
		accordingly. 
AUTHOR : Rijesh Kumar 
DATE : 05/01/15 
*/
/**/

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class StudentList implements Serializable {
    // Version ID checked during deserialization
    private static final long serialVersionUID = 1L;
    private ArrayList<Student> students = new ArrayList<Student>();
    private static StudentList studentList;

    /**/
    /*
    NAME :  StudentList
    SYNOPSIS : 
             
    DESCRIPTION :
			 Constructor is private so that it cannot be called directly.
    		 Must be called through instance method.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    private StudentList() {
    }

    /**/
    /*
    NAME :  StudentList instance()
    SYNOPSIS : 
             
    DESCRIPTION :
			 Singleton to insure only one and only one CourseList instance created.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public static StudentList instance() {
        if (studentList == null) {
            return (studentList = new StudentList());
        } else {
            return studentList;
        }
    }

    /**/
    /*
    NAME :  retrieve
    SYNOPSIS : 
             String StudentID  --> select the student from the give studentID
    DESCRIPTION :
			 retrieve the student from the list   
    RETURNS : return the student if the student id is valid otherwise return null
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public Student retrieve(int studentID) {
        for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
            Student student = (Student) iterator.next();
            if (student.getId() == studentID) {
                return student;
            }
        }
        return null; // End of the list, return null.
    }

    /**/
    /*
    NAME : remove
    SYNOPSIS : 
             String studentID  --> select the student from the give studetnID
    DESCRIPTION :
			 remove the student from the list   
    RETURNS : return true if the student id is valid after removing the student otherwise return false
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public boolean remove(int studentID) {

        int count = 0;
        for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
            Student student = (Student) iterator.next();
            if (student.getId() == studentID) {
                iterator.remove();
                return true;
            }

        }
        return false;
    }

    /**/
    /*
    NAME : insert
    SYNOPSIS : 
             Student student  --> the student object to be inserted to the list
    DESCRIPTION :
			 adds a new student object to the list    
    RETURNS : return true if the student id is valid and adds a new student
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public boolean insert(Student student) {
        students.add(student);
        return true;
    }

    /**/
    /*
    NAME : getStudents
    SYNOPSIS : 
            
    DESCRIPTION :
			the method returs the list of all students in the form of an iterator    
    RETURNS : course iterator containing the list of all students
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public Iterator<Student> getStudents() {
        return students.iterator();
    }

    /**/
    /*
    NAME :  writeObject
    SYNOPSIS :   
    	     ObjectOutputStream output	--> the object as passed in as parameter by the save method to write to the file. 
    DESCRIPTION :
			method that writes out to a file all the StudentList data.     
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    private void writeObject(java.io.ObjectOutputStream output) {
        try {
            output.defaultWriteObject();
            output.writeObject(studentList);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
    
    /**/
    /*
    NAME :  readObject
    SYNOPSIS :   
    	     ObjectInputStream input	--> the read object as passed in as parameter by the retrieve method. 
    DESCRIPTION :
			method that that reads in all Studentlist data from a file    
    RETURNS : void. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    private void readObject(java.io.ObjectInputStream input) {
        try {
            if (studentList != null) {
                return;
            } else {
                input.defaultReadObject();
                if (studentList == null) {
                    studentList = (StudentList) input.readObject();
                } else {
                    input.readObject();
                }
            }
        } catch (IOException ioe) {
            System.out.println("in StudentList readObject \n" + ioe);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    /**/
    /*
    NAME :  toString
    SYNOPSIS : 
    	             
    DESCRIPTION :
			The function converts the details of the students into string.
    RETURNS : String value of the student info. 
    AUTHOR : Rijesh Kumar 
    DATE : 05/01/15 
    */
    /**/
    public String toString() {
        return students.toString();
    }
}
