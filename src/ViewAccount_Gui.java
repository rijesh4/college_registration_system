/**/
/*
CLASS NAME :  ViewAccount_Gui
         
DESCRIPTION :
		This class is responsible for creating screen where a student can view his/her account information.  
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ViewAccount_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_accountPanel;
    private JLabel m_heading;
    private JLabel m_studentIDLabel;
    private JLabel m_nameLabel;
    private JLabel m_addressLabel;
    private JLabel m_phoneLabel;
    private JLabel m_amountDueLabel;
    private JLabel m_holdsLabel;
    private JButton m_accountBackButton;
    //private JTextField result;

    /**/
    /*
    NAME : ViewAccount_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
             Student student 			--> student object from the college instance
             String  studentID			--> stores student's Id from the student object. 
             String  name				--> stores student's name from the student object.
             String phone 				--> stores student's Phone number from the student object.
             String amountDue			--> stores student's current amount due from the student object.
             String holds				--> stores student's current holds from the student object.
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public ViewAccount_Gui(Container window) {

        currentStateNo = ACCOUNT;
        this.window = window;
        m_accountPanel = new JPanel();
        m_accountPanel.setLayout(null);

        m_heading = new JLabel("ACCONT INFORMATION");
        m_heading.setBounds(10, 10, 250, 30);
        m_accountPanel.add(m_heading);

        Student student = college.instance().getStudent(STUDENT_ID);

        String studentID = "Student ID: " + student.getId();
        ;
        String name = "Name: " + student.getName();
        String address = "Address: " + student.getAddress();
        String phone = "Phone: " + student.getPhone();
        String amountDue = "Amount Due: " + student.getAmountDue();
        String holds = "Holds: ";
        if (student.isHolds())
            holds += "Yes";
        else
            holds += "No";

        m_studentIDLabel = new JLabel(studentID);
        m_studentIDLabel.setBounds(10, 100, 400, 30);
        m_accountPanel.add(m_studentIDLabel);

        m_nameLabel = new JLabel(name);
        m_nameLabel.setBounds(10, 130, 400, 30);
        m_accountPanel.add(m_nameLabel);

        m_addressLabel = new JLabel(address);
        m_addressLabel.setBounds(10, 160, 400, 30);
        m_accountPanel.add(m_addressLabel);

        m_phoneLabel = new JLabel(phone);
        m_phoneLabel.setBounds(10, 190, 400, 30);
        m_accountPanel.add(m_phoneLabel);

        m_amountDueLabel = new JLabel(amountDue);
        m_amountDueLabel.setBounds(10, 220, 400, 30);
        m_accountPanel.add(m_amountDueLabel);

        m_holdsLabel = new JLabel(holds);
        m_holdsLabel.setBounds(10, 250, 400, 30);
        m_accountPanel.add(m_holdsLabel);


        m_accountBackButton = new JButton("Back");
        m_accountBackButton.setBounds(10, 610, 150, 30);
        m_accountBackButton.addActionListener(this);
        m_accountPanel.add(m_accountBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_accountPanel);
        m_accountPanel.setVisible(true);
        setTitle("College Registration System - View Account Information");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
                 
    DESCRIPTION :
    		The function handles the onClick events for the Student view account screen to 
    		displays the student account info in the base screen. The event also handles the backButton which
    		takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(m_accountBackButton) ) {
            m_accountPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
