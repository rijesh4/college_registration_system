/**/
/*
CLASS NAME :  Register_Gui
         
DESCRIPTION :
		This class is responsible for creating screen for registration for a specific section in a given course for a student. 
		Registrar/clerk/student can all access the screen based on their authorized transitions. 
		 The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Register_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_registerPanel;
    private JTextField m_resultTextField;

    private JTextField m_sectionIDTextField;
    private JLabel m_sectionIDLabel;
    private JButton m_registerButton;
    private JButton m_registerBackButton;

    /**/
    /*
    NAME : Register_Gui 
    SYNOPSIS :
             Container window   		--> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public Register_Gui(Container window) {

        currentStateNo = REGISTER;
        this.window = window;
        m_registerPanel = new JPanel();
        m_registerPanel.setLayout(null);

        m_sectionIDTextField = new JTextField();
        m_sectionIDTextField.setBounds(10, 110, 150, 30);
        m_registerPanel.add(m_sectionIDTextField);

        m_sectionIDLabel = new JLabel("Section ID");
        m_sectionIDLabel.setBounds(200, 110, 150, 30);
        m_registerPanel.add(m_sectionIDLabel);

        m_registerButton = new JButton("Register");
        m_registerButton.setBounds(10, 160, 150, 30);
        m_registerButton.addActionListener(this);
        m_registerPanel.add(m_registerButton);

        m_resultTextField = new JTextField();
        m_resultTextField.setBounds(10, 220, 300, 300);
        m_registerPanel.add(m_resultTextField);

        m_registerBackButton = new JButton("Back");
        m_registerBackButton.setBounds(10, 610, 150, 30);
        m_registerBackButton.addActionListener(this);
        m_registerPanel.add(m_registerBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void display() {
        window.add(m_registerPanel);
        m_registerPanel.setVisible(true);
        setTitle("College Registration System - Register for a Section");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            String sectionID  			--> input value from the sectionIdTextField
    		int result 					--> stores the value returned from the method registerForSection from the college instance. 
    		      							The value being 0,1,2,3 representing  success, inavailability of section, account hold, and 
    		      							error respectively. 
    		String message 				--> message to be displayed.      
    DESCRIPTION :
    		The function handles the onClick events for the Register for a section screen. The following being the events:
    		register: register for the selected section. 
    		backButton: The event takes the user to the previous screen.
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public void actionPerformed(ActionEvent e) {


        if (e.getSource().equals(m_registerButton) ) {
            //REGISTER FOR CLASS AND DISPLAY RESULTS
            String sectionID = m_sectionIDTextField.getText();

            int result = college.instance().registerForSection(STUDENT_ID, sectionID);

            String message = "";

            if (result == 0)
                message += "You were successfully registered for the section.";
            else if (result == 2)
                message += "Not registered.  The section does not exist.";
            else if (result == 3)
                message += "Not registered.  You have holds.";
            else
                message += "You were not able to register for the section.";

            m_resultTextField.setText(message);
        }

        if (e.getSource().equals(m_registerBackButton) ) {
            m_registerPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
