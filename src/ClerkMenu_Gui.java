/**/
/*
CLASS NAME :  ClerkMenu_Gui
         
DESCRIPTION :
		This is the clerk GUI class handles various duties for a clerk to perform.
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;


public class ClerkMenu_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;

    Container window;

    private JPanel m_clerkMenuPanel;

    private JButton m_editGradeButton;
    private JButton m_massEditGradeButton;
    private JButton m_addStudentButton;
    private JButton m_sectionRosterButton;
    private JButton m_loginAsStudentButton;
    private JButton m_clerkLogoutButton;

    /**/
    /*
    NAME : ClerkLogin_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public ClerkMenu_Gui(Container window) {

        currentStateNo = CLERK_PANEL;
        this.window = window;
        m_clerkMenuPanel = new JPanel();
        m_clerkMenuPanel.setLayout(null);

        m_addStudentButton = new JButton("Add Student");
        m_addStudentButton.setBounds(10, 10, 200, 30);
        m_addStudentButton.addActionListener(this);
        m_clerkMenuPanel.add(m_addStudentButton);

        m_sectionRosterButton = new JButton("Section Roster");
        m_sectionRosterButton.setBounds(10, 60, 200, 30);
        m_sectionRosterButton.addActionListener(this);
        m_clerkMenuPanel.add(m_sectionRosterButton);

        m_editGradeButton = new JButton("Edit Grade");
        m_editGradeButton.setBounds(10, 110, 200, 30);
        m_editGradeButton.addActionListener(this);
        m_clerkMenuPanel.add(m_editGradeButton);


        m_massEditGradeButton = new JButton("Edit Grade for Section");
        m_massEditGradeButton.setBounds(10, 160, 200, 30);
        m_massEditGradeButton.addActionListener(this);
        m_clerkMenuPanel.add(m_massEditGradeButton);

        m_loginAsStudentButton = new JButton("Login as Student");
        m_loginAsStudentButton.setBounds(10, 210, 200, 30);
        m_loginAsStudentButton.addActionListener(this);
        m_clerkMenuPanel.add(m_loginAsStudentButton);

        m_clerkLogoutButton = new JButton("Logout");
        m_clerkLogoutButton.setBounds(10, 610, 200, 30);
        m_clerkLogoutButton.addActionListener(this);
        m_clerkMenuPanel.add(m_clerkLogoutButton);
    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        this.setTitle("College Registration System - Clerk Menu");
        window.add(m_clerkMenuPanel);
        m_clerkMenuPanel.setVisible(true);
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            	      	    
    DESCRIPTION :
    		The function handles the onClick events for the clerkMenu screen. The following being the events:
    		addStudentButton: takes to the activity which is responsible for adding a new student to the system. 
    		sectionRosterButton: takes to the activity to manage a course section informations
    		editGrade: edit grade for a student 
    		massEditGrade: edit grade of students registered for a section 
    		loginAsStudent: authority of a clerk to access a students personal account 
    		clerkLogOut: log out
    		
    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/  
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals( m_addStudentButton)) {
            m_clerkMenuPanel.setVisible(false);
            exitCode = 0;
            GUI.instance().addTransition(currentStateNo, exitCode, ADD_STUDENT);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_sectionRosterButton) ) {
            m_clerkMenuPanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, SECTION_ROSTER);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_editGradeButton) ) {
            m_clerkMenuPanel.setVisible(false);
            exitCode = 3;
            GUI.instance().addTransition(currentStateNo, exitCode, EDIT_GRADE);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_massEditGradeButton)) {
            m_clerkMenuPanel.setVisible(false);
            exitCode = 4;
            GUI.instance().addTransition(currentStateNo, exitCode, MASS_EDIT_GRADE);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_loginAsStudentButton) ) {
            GUI.instance().clerkAsStudentFlag = true;
            m_clerkMenuPanel.setVisible(false);

            exitCode = 5;
            GUI.instance().addTransition(currentStateNo, exitCode, CLERK_AS_STUDENT);
            GUI.instance().changeState(currentStateNo, exitCode);
        }

        if (e.getSource().equals(m_clerkLogoutButton) ) {
            m_clerkMenuPanel.setVisible(false);

            if (GUI.instance().registrarFlag) {
                GUI.instance().registrarFlag = false;
                exitCode = 6;
                GUI.instance().addTransition(currentStateNo, exitCode, REGISTRAR_PANEL);
            } else {
                exitCode = 7;
                GUI.instance().addTransition(currentStateNo, exitCode, MAIN_PANEL);
            }
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }
}
