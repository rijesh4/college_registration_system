/**/
/*
CLASS NAME :  SECTION
         
DESCRIPTION :
		The class Section is responsible for holding a Section data, and interact with 
		College class when needed to update the UI. It also adds/removes a new students
		to a section.
AUTHOR : Rijesh Kumar 
DATE : 05/04/15 
*/
/**/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;


public class Section implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1442747952382251311L;
	private String m_courseName;
    private String m_courseID;
    private String m_sectionID;
    private String m_facultyName;
    private String m_time;
    private String m_location;
    private String m_semester;
    private String m_year;

    private ArrayList<SectionRoster> m_sectionRoster = new ArrayList<SectionRoster>();
    private static SectionRoster tempSectionRoster;

    /**/
    /*
    NAME :  Section
    SYNOPSIS : 
    	String time				--> time alloted for the section  
    	String location			--> location where the course was given 
    	String facultyName		--> faculty name 
    	String courseId			--> courseId
    	String courseName		--> courseName
    	String semester			--> semester
    	String year				--> year
    	int count				--> number of section for a particular course
             
    DESCRIPTION :
			The Constructor is responsible for creating a new section object .  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Section(String time, String location, String facultyName, String courseID, String courseName, String semester
            , String year, int count) {
        this.m_time = time;
        this.m_location = location;
        this.m_facultyName = facultyName;
        this.m_sectionID = courseID + "-" + (count);
        this.m_courseID = courseID;
        this.m_courseName = courseName;
        this.m_semester = semester;
        this.m_year = year;
    }
    
    /**/
    /*
    NAME : insertStudent
    SYNOPSIS : 
             int studentId  --> student to be added  to current section
             String name	--> name
    DESCRIPTION :
			 inserts a student to the section in the sectionRoster    
    RETURNS : return true if the section id is valid and adds a new student to the roster
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean insertStudent(int studentID, String name) {
        /*tempSectionRoster=null;
     tempSectionRoster.setStudentID(studentID);
        tempSectionRoster.setGrade(grade);
        */
        m_sectionRoster.add(new SectionRoster(studentID, name));

        return true;
    }

    /**/
    /*
    NAME : remove
    SYNOPSIS : 
             String studentID  --> the student to be added to the current section
    DESCRIPTION :
			 remove the student form the section   
    RETURNS : return true if the student id is valid after removing the course otherwise return false
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public boolean removeStudent(int studentID) {
        for (Iterator<SectionRoster> iterator = m_sectionRoster.iterator(); iterator.hasNext();) {
            tempSectionRoster = (SectionRoster) iterator.next();
            if (tempSectionRoster.getStudentID() == studentID) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    /**/
    /*
    NAME : returnStudentListIterator
    SYNOPSIS : 
            
    DESCRIPTION :
			the method returns the sectionRoster iterator which in turn is used to get the studentListIterator    
    RETURNS : SectionRoster iterator
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public Iterator<SectionRoster> returnStudentListIterator() {

        Iterator<SectionRoster> iterator = m_sectionRoster.iterator();
        return iterator;
    }

    /**/
    /*
    NAME :  getSemester
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_semester.  
    RETURNS : string m_semester.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getSemester() {
        return m_semester;
    }

    /**/
    /*
    NAME :  setSemester
    SYNOPSIS : 
    	   String semester.          
    DESCRIPTION :
			setter for m_semester.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setSemester(String semester) {
        this.m_semester = semester;
    }

    /**/
    /*
    NAME :  getYear
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_year.  
    RETURNS : String m_year.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getYear() {
        return m_year;
    }

    /**/
    /*
    NAME :  setYear
    SYNOPSIS : 
    	      String year.       
    DESCRIPTION :
			setter for m_year.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setYear(String year) {
        this.m_year = year;
    }

    /**/
    /*
    NAME :  getTime
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_time.  
    RETURNS : String m_time.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getTime() {
        return m_time;
    }

    /**/
    /*
    NAME :  setTime
    SYNOPSIS : 
    	      String time.       
    DESCRIPTION :
			setter for m_time.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setTime(String time) {
        this.m_time = time;
    }

    /**/
    /*
    NAME :  getLocation
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_Location.  
    RETURNS : String m_Location.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getLocation() {
        return m_location;
    }

    /**/
    /*
    NAME :  setLocation
    SYNOPSIS : 
    	      String Location.        
    DESCRIPTION :
			setter for m_Location.  
    RETURNS :
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setLocation(String location) {
        this.m_location = location;
    }

    /**/
    /*
    NAME :  getfacultyName
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_facultyName.  
    RETURNS : String m_facultyName.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getFacultyName() {
        return m_facultyName;
    }

    /**/
    /*
    NAME :  setfacultyName
    SYNOPSIS : 
    	      String facultyName.       
    DESCRIPTION :
			setter for m_facultyName.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setFacultyName(String facultyName) {
        this.m_facultyName = facultyName;
    }

    /**/
    /*
    NAME :  getSectionID
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_sectionID.  
    RETURNS : String m_sectionID.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getSectionID() {
        return m_sectionID;
    }

    /**/
    /*
    NAME :  setSectionID
    SYNOPSIS : 
    	      String sectionID.       
    DESCRIPTION :
			setter for m_sectionID.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setSectionID(String sectionID) {
        this.m_sectionID = sectionID;
    }

    /**/
    /*
    NAME :  getcourseID
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_courseID.  
    RETURNS : String m_courseID.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getCourseID() {
        return m_courseID;
    }

    /**/
    /*
    NAME :  setcourseID
    SYNOPSIS : 
    	   String courseID.          
    DESCRIPTION :
			setter for m_courseID.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setCourseID(String courseID) {
        this.m_courseID = courseID;
    }

    /**/
    /*
    NAME :  getcourseName
    SYNOPSIS : 
    	             
    DESCRIPTION :
			getter for m_courseName.  
    RETURNS : String m_courseName.
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public String getCourseName() {
        return m_courseName;
    }

    /**/
    /*
    NAME :  setcourseName
    SYNOPSIS : 
    	      String courseName.       
    DESCRIPTION :
			setter for m_courseName.  
    RETURNS : 
    AUTHOR : Rijesh Kumar 
    DATE : 05/04/15 
    */
    /**/
    public void setCourseName(String courseName) {
        this.m_courseName = courseName;
    }

    @Override
    public String toString() {
        return
                "courseName='" + m_courseName + '\'' +
                        ", courseID='" + m_courseID + '\'' +
                        ", sectionID='" + m_sectionID + '\'' +
                        ", facultyName='" + m_facultyName + '\'' +
                        ", time='" + m_time + '\'' +
                        ", location='" + m_location + '\'' +
                        ", semester='" + m_semester + '\'' +
                        ", year='" + m_year + '\''

                ;
    }
}
