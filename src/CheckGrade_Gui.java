/**/
/*
CLASS NAME :  CheckGrade_Gui
         
DESCRIPTION :
		This is the GUI class that creates the screen to allows to check a student's grade. The cheking
		of grade is accesible from student/clerk/registrar gui. 
		The class handles creating required window,panel, buttons etc .. and is responsible for 
		handling events by the user.   
AUTHOR : Rijesh Kumar 
DATE : 04/25/15  
*/
/**/

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CheckGrade_Gui extends HeadGui implements ActionListener {
    private static final long serialVersionUID = 1L;
    Container window;

    private JPanel m_checkGradePanel;
    private JTextField m_result;

    private JTextField m_sectionIDTextField;
    private JLabel m_sectionIDLabel;
    private JButton m_checkGradeButton;
    private JButton m_checkGradeBackButton;

    /**/
    /*
    NAME : CheckGrade_Gui 
    SYNOPSIS :
            Container window   --> window to which the GUI elements need to be added. 
    DESCRIPTION :
			The Constructor is responsible for creating the initial elements of the current GUI. 
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/     
    public CheckGrade_Gui(Container window) {

        currentStateNo = CHECKGRADE;
        this.window = window;
        m_checkGradePanel = new JPanel();
        m_checkGradePanel.setLayout(null);

        m_sectionIDTextField = new JTextField();
        m_sectionIDTextField.setBounds(10, 110, 150, 30);
        m_checkGradePanel.add(m_sectionIDTextField);

        m_sectionIDLabel = new JLabel("Section ID");
        m_sectionIDLabel.setBounds(200, 110, 150, 30);
        m_checkGradePanel.add(m_sectionIDLabel);

        m_checkGradeButton = new JButton("Check Grade");
        m_checkGradeButton.setBounds(10, 160, 150, 30);
        m_checkGradeButton.addActionListener(this);
        m_checkGradePanel.add(m_checkGradeButton);

        m_result = new JTextField();
        m_result.setBounds(10, 220, 300, 300);
        m_checkGradePanel.add(m_result);

        m_checkGradeBackButton = new JButton("Back");
        m_checkGradeBackButton.setBounds(10, 610, 150, 30);
        m_checkGradeBackButton.addActionListener(this);
        m_checkGradePanel.add(m_checkGradeBackButton);

    }

    /**/
    /*
    NAME : Display
    SYNOPSIS :
            window --> get swing Jframe window of current pane from container initiated by constructor. 
    DESCRIPTION :
			The function that sets up the current Panel with current window (instantiated by the constructor),
			setting its visibility and updating its title.  
    RETURNS : None.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/    
    public void display() {
        window.add(m_checkGradePanel);
        m_checkGradePanel.setVisible(true);
        setTitle("College Registration System - Check Grade");
    }

    /**/
    /*
    NAME : actionPerformed
    SYNOPSIS :
            	boolean found  					--> check if the sectionId  exists in transcript 
            	String grade 					--> stores grade from the transcript 
            	String sectionID				--> gets the sectionId from the transcript of a student using 
            										student's student ID. 
            	Iterator<Transcript> iterator 	--> stores the Transcript elements to be traversed to find the 
            	 								    individual elements. 
            	
                    
    DESCRIPTION :
    		The function is responsible for catching the checkGrade button event. It 
    		takes the student Id and gets the list of elements in the transcript to 
    		traverse to find the grade of a particular section.    

    RETURNS : void.
    AUTHOR : Rijesh Kumar 
    DATE : 04/25/15 
    */
    /**/   
    public void actionPerformed(ActionEvent e) {


        if (e.getSource().equals(m_checkGradeButton) ) {
            String grade = "";
            boolean found = false;
            String sectionID = m_sectionIDTextField.getText();
            Iterator<Transcript> iterator = college.getTranscriptIterator(STUDENT_ID);

            while (iterator.hasNext()) {
                Transcript tempTranscript = (Transcript) iterator.next();
                if (tempTranscript.getSectionID().equals(sectionID)) {
                    found = true;
                    grade += "You received a grade of " + tempTranscript.getGrade()
                            + " in the section " + sectionID + ".";
                    break;
                }
            }
            if (found)
                m_result.setText(grade);
            else
                m_result.setText("That section is NOT on your transcript.");
        }

        if (e.getSource().equals(m_checkGradeBackButton) ) {
            m_checkGradePanel.setVisible(false);
            exitCode = 1;
            GUI.instance().addTransition(currentStateNo, exitCode, STUDENT_PANEL);
            GUI.instance().changeState(currentStateNo, exitCode);
        }
    }

}
